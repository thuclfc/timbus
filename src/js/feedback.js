var preMenuTag = 'Timbus';
function onClickMenuItem(tag){
	window.location = tag;
};

function onClickMenuItemBK(tag){
	var cmn = document.getElementById('mn-' + tag);
	if (cmn != null){
		if (cmn.className.indexOf('-selected') < 0){
			if (preMenuTag.length > 0) {
				var pmn = document.getElementById('mn-' + preMenuTag);
				if (pmn != null)
					pmn.className = 'feedback-menuitem-none';
				var ptb = document.getElementById('tb-' + preMenuTag);
				if (ptb != null)
					ptb.style.display = 'none';
			}
		
			cmn.className = 'feedback-menuitem-selected';
			var ctb = document.getElementById('tb-' + tag);
			if (ctb != null)
				ctb.style.display = '';
			if(tag == 'Timbus'){
				EnableValidatorFeedbackTimbus(true);
				EnableValidatorFeedbackHanoibus(false);
				EnableValidatorFeedbackStatistics(false);
			}else if(tag == 'Hanoibus'){
				EnableValidatorFeedbackTimbus(false);
				EnableValidatorFeedbackHanoibus(true);
				EnableValidatorFeedbackStatistics(false);
			}else if(tag == 'Statistics'){
				EnableValidatorFeedbackTimbus(false);
				EnableValidatorFeedbackHanoibus(false);
				EnableValidatorFeedbackStatistics(true);
			}

			preMenuTag = tag;
		}
		
		EnableValidatorFeedbackTimbus(true);
		EnableValidatorFeedbackHanoibus(false);
		EnableValidatorFeedbackStatistics(false);
	}
};

function EnableValidatorFeedbackTimbus(state){
	ValidatorEnable(document.getElementById('rfvTBFullName'), state);
	ValidatorEnable(document.getElementById('rfvTBEmail'), state);
	ValidatorEnable(document.getElementById('revTBEmail'), state);
	ValidatorEnable(document.getElementById('rfvTBSpeedData'), state);
	ValidatorEnable(document.getElementById('rfvTBFunctionUsed'), state);
	ValidatorEnable(document.getElementById('rfvTBExactlySuggest'), state);
	ValidatorEnable(document.getElementById('rfvTBContent'), state);
};

function EnableValidatorFeedbackHanoibus(state){
	ValidatorEnable(document.getElementById('rfvHNBFullName'), state);
	ValidatorEnable(document.getElementById('rfvHNBEmail'), state);
	ValidatorEnable(document.getElementById('revHNBEmail'), state);
	ValidatorEnable(document.getElementById('rfvHNBDate'), state);
	ValidatorEnable(document.getElementById('rfvHNBTime'), state);
	ValidatorEnable(document.getElementById('rfvHNBFleet'), state);
	ValidatorEnable(document.getElementById('rfvHNBVehiclePlate'), state);
	ValidatorEnable(document.getElementById('rfvHNBProblem'), state);
	ValidatorEnable(document.getElementById('rfvHNBContent'), state);
};

function EnableValidatorFeedbackStatistics(state){
	ValidatorEnable(document.getElementById('rfvQuestion01'), state);
	ValidatorEnable(document.getElementById('rfvQuestion02'), state);
	ValidatorEnable(document.getElementById('rfvQuestion03'), state);
	ValidatorEnable(document.getElementById('rfvQuestion04'), state);
	ValidatorEnable(document.getElementById('rfvQuestion05'), state);
	ValidatorEnable(document.getElementById('rfvQuestion06'), state);
	ValidatorEnable(document.getElementById('rfvQuestion07'), state);
};