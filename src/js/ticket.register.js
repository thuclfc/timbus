﻿/**
 * Đối tượng đăng ký vé tháng
 * Tác giả: SonKT
 * Ngày tạo: 21/03/2018
 * Mô tả: Đối tượng này dùng để quản lý việc đăng ký vé tháng
 * Ngày update cuối: 11/04/2018
 * Người Update: SonKT
 */
//"use strict";
/**
 * Đối tượng đăng ký vé
 */
function DangKyVe() {
    this.AnhThe = null;                             //! Lưu thông tin ảnh thẻ từ Filemanager trả về
    this.MatTruoc = null;                           //! Lưu thông tin ảnh chứng minh nhân dân hoặc ảnh thẻ công nhân hoặc thẻ sinh viên mặt trước từ filemanager trả về
    this.MatSau = null;                             //! Lưu thông tin ảnh chứng minh nhân dân hoặc ảnh thẻ công nhân hoặc thẻ sinh viên mặt sau từ filemanager trả về
    this.DonDangKy = "";                            //! Lưu link file đăng ký của học sinh/sinh viên
    this.DonXacNhan = "";                           //! Lưu link file đăng ký của công nhân
    this.DuLieuAnhThe = "";                         //! Dữ liệu ảnh thẻ để upload ảnh lên server
    this.DuLieuMatTruoc = "";                       //! Dữ liệu ảnh mặt trước để upload ảnh lên server
    this.DuLieuMatSau = "";                         //! Dữ liệu ảnh mặt sau để upload ảnh lên server
    this.DuLieuDonDangKy = "";                      //! Dữ liệu ảnh đơn đăng ký để upload ảnh lên server
    this.DuLieuDonXacNhan = "";                     //! Dữ liệu ảnh đơn xác nhận để upload ảnh lên server
    this.HoTen = "";                                //! Họ tên người đăng ký
    this.NgaySinh = new Date();                     //! Ngày sinh người đăng ký
    this.DienThoai = "";                            //! Điện thoại người đăng ký
    this.TruongHoc = "";                            //! ID Trường học của sinh viên hoặc học sinh
    this.TenTruongHoc = "";                         //! Tên trường học
    this.NienKhoa = "";                             //! Niên khóa của sinh viên hoặc học sinh
    this.KhuCongNghiep = "";                        //! ID khu công nghiệp
    this.TenKhuCongNghiep = "";                     //! Tên khu công nghiệp
    this.DiaChi = "";                               //! Địa chỉ của người đăng ký
    this.TenDiaChi = "";                               //! Địa chỉ của người đăng ký
    this.DoiTuongUuTien = 0;                        //! Loại đối tượng ưu tiên: 0 - bình thường, 1 - Học Sinh, 2 - Công nhân, 3 - Người cao tuổi
    this.LoaiThe = 1;                               //! Loại thẻ 1 - Một tuyến, 2 - Liên tuyến
    this.Tuyen = "";                                //! Mã tuyến
    this.NoiNhanThe = "";                           //! Mã nơi nhận thẻ
    this.NgayNhanThe = new Date();                  //! Ngày nhận thẻ
    this.ThangDangKyMua = [];                       //! Các tháng đăng ký mua
    this.Quan = "";                                 //! Mã quận khi đăng ký liên tuyến
    this.NhanTaiNha = false;                        //! Đăng ký nhận tại nhà True - Nhận tại nhà, False - Không nhận tại nhà
    this.DiaChiNhanThe = "";                        //! Địa chỉ nhận thẻ
    this.ClientID = "";                             //! ID của thiết bị nếu là App mobile
    this.Dialog = null;                             //! Dialog của cửa sổ chỉnh sửa hình ảnh
    this.TrangThaiVe = [];                          //! Trạng thái vé
    this.MaTicket = "";                             //! Mã vé dùng khi update hoặc hủy vé
    this.TuyChon = null;                            //! Tùy chọn được gán giá trị khi khởi tạo đối tượng
    this.TrangThaiCu = 0;                           //! Trạng thái trước, được sử dụng trong khi sửa hoặc hủy
    this.BaseImageUrl = "http://localhost:802/images/register/"; //! Url cơ sở cho hình ảnh
    this.IsRedirect = false;                        //! Dùng để kiểm tra xem có phải là load từ redirect hay không
}
/**
 * Hàm khởi tạo object
 * @param {any} tuyChon là đối tượng chứa thông tin khởi tạo bao gồm các thuộc tính
 */
DangKyVe.prototype.khoiTao = function (tuyChon) {
    var that = this;
    try {
        //! Cuộn windows
        $(window).scroll(function () {
            if ($(this).scrollTop() !== 0) {
                $('#toTop').fadeIn();
            } else {
                $('#toTop').fadeOut();
            }
        });

        $('.mauDon').hide();

        $('#toTop').click(function () {
            $('body,html').animate({ scrollTop: 0 }, 800);
        });

        if (tuyChon.baseImageUrl) {
            that.BaseImageUrl = tuyChon.baseImageUrl;
        }
        if (tuyChon) {
            that.IsRedirect = tuyChon.isRedirect;
        }
        var thongTinThangMuaTem = [];
        if (tuyChon) {
            that.TuyChon = tuyChon;
        }
        var length = tuyChon.danhSachTrangThaiVe.length;
        that.TrangThaiVe.push(' ');
        if (tuyChon.danhSachTrangThaiVe) {
            for (var j = 0; j < (length); j++) {
                that.TrangThaiVe.push(tuyChon.danhSachTrangThaiVe[j]);
            }
        }
        //! Lấy về ngày nhận thẻ khống chế
        var ngayKhongChe = that.TinhNgayNhanThe(tuyChon);

        $('#lnkDieuKhoan').click(function () {
            if ($('#termsandcon').hasClass('hidden')) {
                $('#termsandcon').removeClass('hidden');
            } else {
                $('#termsandcon').addClass('hidden');
            }
        });


        //! Tính tháng mua tem
        if (tuyChon && tuyChon.ngayBatDauThang) {
            thongTinThangMuaTem = that.TinhThangMuaTem(tuyChon.ngayBatDauThang);
        } else {
            thongTinThangMuaTem = that.TinhThangMuaTem(25);
        }

        //! Gán giá trị tháng mua tem vào control
        for (var i = 0; i < thongTinThangMuaTem.length; i++) {
            var obj = $("#chkThangThu" + (i + 1));
            obj.val(thongTinThangMuaTem[i]);
            $('label[for="chkThangThu' + (i + 1) + '"]').text("Tháng " + thongTinThangMuaTem[i]);
        }

        //! Sự kiện thay đổi checkbox tháng mua
        $(".thangTem").change(function () {
            var index = that.ThangDangKyMua.indexOf($(this).val());
            if ($(this).is(':checked')) {
                if (index < 0) {
                    that.ThangDangKyMua.push($(this).val());
                }
            } else {
                if (index >= 0) {
                    that.ThangDangKyMua.splice(index, 1);
                }
            }
            var tienTem = that.TinhPhiTem();
            var tienShip = $('#tienShip').text();

            var tongTien = +tienTem.replace(",", "") + +tienShip.replace(",", "");

            $("#tienTem").text(tienTem + " vnđ");
            $('#tongTien').text(that.DinhDangTien(tongTien + "") + " vnđ");
        });

        //! Event mở Image manager hình ảnh
        $('#btnMatSau').click(function () {
            var urlMatSau = $('#txtAnhCMNDMatSau').val();
            if (urlMatSau === '') {
                that.OpenDialog('/Ctr/ImageManager.aspx?container=imgManager&return=txtAnhCMNDMatSau');
            } else {
                that.OpenDialog('/Ctr/ImageManager.aspx?container=imgManager&return=txtAnhCMNDMatSau&url=' + urlMatSau);
            }
        });
        $('#btnMatTruoc').click(function () {
            var urlMatTruoc = $('#txtAnhCMNDMatTruoc').val();
            if (urlMatTruoc === '') {
                that.OpenDialog('/Ctr/ImageManager.aspx?container=imgManager&return=txtAnhCMNDMatTruoc');
            } else {
                that.OpenDialog('/Ctr/ImageManager.aspx?container=imgManager&return=txtAnhCMNDMatTruoc&url=' +
                    urlMatTruoc);
            }
        });

        $('#btnChonAnhThe').click(function () {
            var urlAnhLamThe = $('#txtAnhLamThe').val();
            if (urlAnhLamThe === '') {
                that.OpenDialog(
                    '/Ctr/ImageManager.aspx?container=imgManager&return=txtAnhLamThe&AspectRatio=0.6666666666666667');
            } else {
                that.OpenDialog(
                    '/Ctr/ImageManager.aspx?container=imgManager&return=txtAnhLamThe&AspectRatio=0.6666666666666667&url=' +
                    urlAnhLamThe);
            }
        });

        $('#btnDonXacNhan').click(function () {
            var urlDonXacNhan = $('#txtDonXacNhan').val();
            if (urlDonXacNhan === '') {
                that.OpenDialog(
                    '/Ctr/ImageManager.aspx?container=imgManager&return=txtDonXacNhan&AspectRatio=0.6666666666666667');
            } else {
                that.OpenDialog(
                    '/Ctr/ImageManager.aspx?container=imgManager&return=txtDonXacNhan&AspectRatio=0.6666666666666667&url=' +
                    urlDonXacNhan);
            }
        });

        $('#btnDonDangKy').click(function () {
            var urlDonDangKy = $('#txtDonDangKy').val();
            if (urlDonDangKy === '') {
                that.OpenDialog(
                    '/Ctr/ImageManager.aspx?container=imgManager&return=txtDonDangKy&AspectRatio=0.6666666666666667');
            } else {
                that.OpenDialog(
                    '/Ctr/ImageManager.aspx?container=imgManager&return=txtDonDangKy&AspectRatio=0.6666666666666667&url=' +
                    urlDonDangKy);
            }
        });

        $('button[data-toggle="popover"]').popover({
            html: true,
            trigger: 'hover',
            content: function () {
                //return $(this).attr('data-content');
                var thisId = $(this)[0].id;
                switch (thisId) {
                    case 'prvChonAnhThe':
                        return "<img src='" + $('#txtAnhLamThe').attr('data-content') + "' style='max-width:200px;'>";
                    case 'prvMatTruoc':
                        return "<img src='" + $('#txtAnhCMNDMatTruoc').attr('data-content') + "' style='max-width:340px;'>";
                    case 'prvMatSau':
                        return "<img src='" + $('#txtAnhCMNDMatSau').attr('data-content') + "' style='max-width:340px;'>";
                    case 'prvDonDangKy':
                        return "<img src='" + $('#txtDonDangKy').attr('data-content') + "' style='max-width:600px;'>";
                    case 'prvDonXacNhan':
                        return "<img src='" + $('#txtDonXacNhan').attr('data-content') + "' style='max-width:600px;'>";
                    default:
                }
            }
        });

        $('.anh-mau').popover({
            html: true,
            trigger: 'hover',
            content: function () {
                return "<img src='" + $(this).attr('data-href') + "' style='max-width:200px;'>";
            },
            title: function () {
                return $(this).data('title') + '<span class="close">&times;</span>';
            }
        }).on('shown.bs.popover',
            function (e) {
                var popover = jQuery(this);
                jQuery(this).parent().find('div.popover .close').on('click',
                    function (e) {
                        popover.popover('hide');
                    });
            });
        //! Cập nhật lại hình ảnh của input ảnh thẻ   
        $('#imgManager').on('dialogclose',
            function (event, ui, xyz) {
                var controlNhanAnh = $(this).attr('data-target');
                switch (controlNhanAnh) {
                    case 'txtAnhLamThe':
                        $("#prvChonAnhThe").removeClass('hidden');
                        that.DinhTyLeAnh($('#txtAnhLamThe'), that.CallBackGanGiaTri, 1);
                        break;
                    case 'txtAnhCMNDMatTruoc':
                        $("#prvMatTruoc").removeClass('hidden');
                        that.DinhTyLeAnh($('#txtAnhCMNDMatTruoc'), that.CallBackGanGiaTri, 2);
                        break;
                    case 'txtAnhCMNDMatSau':
                        $("#prvMatSau").removeClass('hidden');
                        that.DinhTyLeAnh($('#txtAnhCMNDMatSau'), that.CallBackGanGiaTri, 2);
                        break;
                    case 'txtDonDangKy':
                        $("#prvDonDangKy").removeClass('hidden');
                        that.DinhTyLeAnh($('#txtDonDangKy'), that.CallBackGanGiaTri, 3);
                        break;
                    case 'txtDonXacNhan':
                        $("#prvDonXacNhan").removeClass('hidden');
                        that.DinhTyLeAnh($('#txtDonXacNhan'), that.CallBackGanGiaTri, 3);
                        break;
                    default:
                }
            });

        //! Khởi tạo giá trị ban đầu và thiết lập datetimepicker cho 2 control ngày sinh
        $('#dtpNgaySinh').datetimepicker({
            locale: 'vi',
            format: 'DD/MM/YYYY',
            useCurrent: false,
            maxDate: new Date()
        }).on('dp.change',
            function (e) {
                var ngaySinh = e.date.format('DD/MM/YYYY');
                that.KiemTraTuoi(ngaySinh);
                if (that.KiemTraDuLieuTimKiem()) {
                    that.NapDuLieuTimKiem();
                }
            });
        $('#dtpNgayNhanThe').datetimepicker({
            locale: 'vi',
            format: 'DD/MM/YYYY',
            defaultDate: moment(ngayKhongChe.somNhat).format('Y/M/D'),
            minDate: moment(ngayKhongChe.somNhat).format('Y/M/D'),
            maxDate: moment(ngayKhongChe.muonNhat).format('Y/M/D'),
            daysOfWeekDisabled: ngayKhongChe.ngayNghi,
            disabledDates: ngayKhongChe.ngayLe
        });

        //! Click event đối tượng ưu tiên
        $('#rdDoiTuong_UuTien').click(function () {
            //! Gọi hàm hiện combo đối tượng ưu tiên
            that.AnHienComboLoaiDoiTuong(true);
            $('#slDiaChi').parentsUntil('div.row').parent().addClass('hidden');
            var tienTem = that.TinhPhiTem();
            var tienShip = $('#tienShip').text();

            var tongTien = +tienTem.replace(",", "") + +tienShip.replace(",", "");

            $("#tienTem").text(tienTem + " vnđ");
            $('#tongTien').text(that.DinhDangTien(tongTien + "") + " vnđ");

        });
        //! Click event đối tượng bình thường
        $('#rdDoiTuong_BinhThuong').click(function () {
            //! Gọi hàm ẩn combo đối tượng ưu tiên và ẩn các control liên quan đến đối tượng ưu tiên
            that.AnHienComboLoaiDoiTuong(false);
            $('#slDiaChi').parentsUntil('div.row').parent().removeClass('hidden');
            $('#slLoaiDoiTuong').val('0').change();
            var tienTem = that.TinhPhiTem();
            var tienShip = $('#tienShip').text();

            var tongTien = +tienTem.replace(",", "") + +tienShip.replace(",", "");

            $("#tienTem").text(tienTem + " vnđ");
            $('#tongTien').text(that.DinhDangTien(tongTien + "") + " vnđ");
            that.AnToanBo();
        });

        //! Click event chọn 1 tuyến
        $('#rdLoaiThe_MotTuyen').click(function () {
            //! Gọi hàm hiện combo đăng ký 1 tuyến
            that.ControlTuyen(true);
            that.LoaiThe = 1;
            var tienTem = that.TinhPhiTem();
            var tienShip = $('#tienShip').text();

            var tongTien = +tienTem.replace(",", "") + +tienShip.replace(",", "");

            $("#tienTem").text(tienTem + " vnđ");
            $('#tongTien').text(that.DinhDangTien(tongTien + "") + " vnđ");
        });

        //! Click event chọn liên tuyến
        $('#rdLoaiThe_LienTuyen').click(function () {
            //! Gọi hàm hiện combo đăng ký 1 tuyến
            that.ControlTuyen(false);
            that.LoaiThe = 2;
            var tienTem = that.TinhPhiTem();
            var tienShip = $('#tienShip').text();

            var tongTien = +tienTem.replace(",", "") + +tienShip.replace(",", "");

            $("#tienTem").text(tienTem + " vnđ");
            $('#tongTien').text(that.DinhDangTien(tongTien + "") + " vnđ");
        });

        $("#chkNhanTaiNha").change(function () {
            if ($(this).is(':checked')) {
                that.ControlDangKyTaiNha(true);
            }
            else {
                that.ControlDangKyTaiNha(false);
            }
        });
        $('#btnNhapLai').click(function () {
            $('#slTuyen').val('');
            $('#slTuyen').selectpicker('refresh');
            $('#slNoiNhan').val('');
            $('#slNoiNhan').selectpicker('refresh');
            $('#slQuan').val('');
            $('#slQuan').selectpicker('refresh');
        });
        //! Thay đổi loại đối tượng
        $('#slLoaiDoiTuong').change(function () {
            that.DoiTuongUuTien = $(this).val();
            //! Ẩn toàn bộ
            that.AnToanBo();
            $('.mauDon').hide();
            var tienTem = that.TinhPhiTem();
            var tienShip = $('#tienShip').text();

            var tongTien = +tienTem.replace(",", "") + +tienShip.replace(",", "");

            $("#tienTem").text(tienTem + " vnđ");
            $('#tongTien').text(that.DinhDangTien(tongTien + "") + " vnđ");
            //that.XoaBaoLoiSelect($(this));
            $(this).valid();
            //! Kiểm tra loại ưu tiên
            switch (that.DoiTuongUuTien) {
                //! Học sinh sinh viên
                case '1':
                    //! Hiện control cho Học sinh
                    that.ControlHocSinh(true);
                    $('#slDiaChi').parentsUntil('div.row').parent().addClass('hidden');
                    break;
                //! Công nhân khu công nghiệp
                case '2':
                    //! Hiện control cho công nhân
                    that.ControlCongNhan(true);
                    $('#slDiaChi').parentsUntil('div.row').parent().addClass('hidden');
                    $('.mauDon').show();
                    break;
                //! Người cao tuổi
                //case '3': // Bỏ từ ngày 28?08?2019 follow ticket 2926
                //    //! Hiện control cho người cao tuổi
                //    that.ControlNguoiCaoTuoi(true);
                //    $('#slDiaChi').parentsUntil('div.row').parent().removeClass('hidden');
                //    break;
                //! Không chọn gì
                default:
                    $('#slDiaChi').parentsUntil('div.row').parent().removeClass('hidden');
                    break;
            }
        });

        $('#slTruongHoc').change(function () {
            //that.XoaBaoLoiSelect($(this));
            $(this).valid();
        });
        $('#slTuyen').change(function () {
            //that.XoaBaoLoiSelect($(this));
            $(this).valid();
        });
        $('#slDiaChi').change(function () {
            //that.XoaBaoLoiSelect($(this));
            $(this).valid();
        });
        $('#slQuan').change(function () {
            //that.XoaBaoLoiSelect($(this));
            $(this).valid();
        });
        $('#slKhuCongNghiep').change(function () {
            //that.XoaBaoLoiSelect($(this));
            $(this).valid();
        });
        $('#slNoiNhan').change(function () {
            //that.XoaBaoLoiSelect($(this));
            $(this).valid();
        });

        $('#imageManager').on('show.bs.modal', function (e) {
            var loadurl = $(e.relatedTarget).data('load-url');
            $(this).find('.modal-body').load(loadurl);
        });
        //! Hủy hồ sơ
        $('#btnHuyHoSo').click(function () {
            if ($('#txtTicketId').val() === '') {
                $.alert({
                    title: 'Thông báo!',
                    escapeKey: 'OK',
                    columnClass: 'col-md-6 col-md-offset-3',
                    closeIcon: true,
                    content: 'Vui lòng nhập chính xác họ tên, ngày sinh và điện thoại mà bạn đã đăng ký để xác nhận, sau đó nhấn nút hủy đơn',
                    buttons: {
                        OK: {
                            text: 'Tôi đã hiểu',
                            btnClass: 'btn-danger'
                        }
                    }
                });
            } else {
                $.confirm({
                    title: 'Thông báo!',
                    content: '' +
                        '<form action="" class="formName">' +
                        '<div class="form-group">' +
                        '<label>Lý do hủy</label>' +
                        '<input type="text" placeholder="Nhập lý do hủy của bạn" class="form-control" id="txtLyDoHuy" required />' +
                        '</div>' +
                        '</form>',
                    buttons: {
                        formSubmit: {
                            text: 'Gửi',
                            btnClass: 'btn-blue',
                            action: function () {
                                var lyDoHuy = this.$content.find('#txtLyDoHuy').val();
                                if (lyDoHuy.length > 500) {
                                    $.alert('Vui lòng nhập không quá 500 ký tự');
                                    return false;
                                }
                                if (!lyDoHuy) {
                                    $.alert('Vui lòng nhập lý do hủy');
                                    return false;
                                }
                                var formData = new FormData();
                                formData.append("hanhDong", "huyDangKyVe");
                                formData.append("Captcha", $('#txtCaptcha').val());
                                formData.append("MaTicket", $('#txtTicketId').val());
                                formData.append("LyDoHuy", lyDoHuy);
                                formData.append("State", $('#txtOldState').val());
                                $.ajax({
                                    url: "ActionHandler.ashx",
                                    type: "POST",
                                    data: formData,
                                    contentType: false,
                                    processData: false,
                                    success: function (result) {
                                        var ketQua = $.parseJSON(result);
                                        //grecaptcha.reset();
                                        $('#imgCatchaValidate').attr('src',
                                            '/TicketRegister/captchaImg.aspx?ver=' + that.GetRandomText(5));
                                        $("#txtCaptcha").val("");
                                        switch (ketQua.st) {
                                            case 1:
                                                //! Thành công
                                                $.alert({
                                                    title: 'Hoàn thành!',
                                                    escapeKey: 'OK',
                                                    closeIcon: true,
                                                    columnClass: 'col-md-6 col-md-offset-3',
                                                    content: 'Đơn đăng ký của bạn đã được hủy thành công.',
                                                    buttons: {
                                                        OK: {
                                                            text: 'Đóng',
                                                            btnClass: 'btn-danger',
                                                            action: function () {
                                                                location.href = "/Ticket/Default.aspx";
                                                            }
                                                        }
                                                    }
                                                });
                                                break;
                                            case 0:
                                                $.alert({
                                                    title: 'Thông báo!',
                                                    escapeKey: 'OK',
                                                    closeIcon: true,
                                                    columnClass: 'col-md-6 col-md-offset-3',
                                                    content: 'Mã xác thực không chính xác! Quý khách vui lòng kiểm tra lại',
                                                    buttons: {
                                                        OK: {
                                                            text: 'Đóng',
                                                            btnClass: 'btn-danger'
                                                        }
                                                    }
                                                });
                                                break;
                                            case -1:
                                                //! trường hợp thay đổi trạng thái
                                                if (ketQua.isStateChange !== 0) {
                                                    $.alert({
                                                        title: 'Thông báo!',
                                                        escapeKey: 'OK',
                                                        closeIcon: true,
                                                        columnClass: 'col-md-6 col-md-offset-3',
                                                        content: 'Đơn của quý khách đăng ký ngày <span class="text-red">' + $('#txtCreatedDate').val() + '</span> có thể vừa được chuyển sang trạng thái <span class="text-red"> ' + that.TrangThaiVe[ketQua.isStateChange] + '</span>.<br/>Quý khách vui lòng liên hệ tới số hotline <span class="text-red">1900 1296</span> để được hỗ trợ',
                                                        buttons: {
                                                            OK: {
                                                                text: 'Tôi đã hiểu',
                                                                btnClass: 'btn-danger'
                                                            }
                                                        }
                                                    });
                                                    return;
                                                }
                                                $.alert({
                                                    title: 'Thông báo!',
                                                    escapeKey: 'OK',
                                                    closeIcon: true,
                                                    columnClass: 'col-md-6 col-md-offset-3',
                                                    content: ketQua.msg,
                                                    buttons: {
                                                        OK: {
                                                            text: 'Tôi đã hiểu',
                                                            btnClass: 'btn-danger',
                                                            action: function () {
                                                                $('#btnNhapLai').trigger('click');
                                                            }
                                                        }
                                                    }
                                                });
                                                break;
                                            case -3:
                                                //! Đơn đăng ký đã được hủy.
                                                var msg = 'Đơn đăng ký của quý khách đã được hủy.';
                                                $.alert({
                                                    title: 'Thông báo!',
                                                    escapeKey: 'OK',
                                                    closeIcon: true,
                                                    columnClass: 'col-md-6 col-md-offset-3',
                                                    content: msg,
                                                    buttons: {
                                                        OK: {
                                                            text: 'Tôi đã hiểu',
                                                            btnClass: 'btn-danger',
                                                            action: function () {
                                                                $('#btnNhapLai').trigger('click');
                                                                location.reload();
                                                            }
                                                        }
                                                    }
                                                });

                                                break;
                                        }
                                    },
                                    error: function (xhr, status) {
                                        console.log("Có lỗi xảy ra: " + status);
                                    }
                                });
                                return true;
                            }
                        },
                        cancel: {
                            text: 'Đóng lại'
                        }
                    },
                    onContentReady: function () {
                        // bind to events
                        var jc = this;
                        this.$content.find('form').on('submit', function (e) {
                            // if the user submits the form by pressing enter in the field.
                            e.preventDefault();
                            jc.$$formSubmit.trigger('click'); // reference the button and click it
                        });
                    }
                });
            }
        });

        that.DangKyNutThem();

        var tienTem = that.TinhPhiTem();
        var tienShip = $('#tienShip').text();

        var tongTien = +tienTem.replace(",", "") + +tienShip.replace(",", "");

        $("#tienTem").text(tienTem + " vnđ");
        $('#tongTien').text(that.DinhDangTien(tongTien + "") + " vnđ");
        //! Thiết lập tab mở mặc định
        //that.OpenTab($('#dangKyVeThang'), $('#dangKy'));

        $('#txtHoTen').change(function () {
            if (that.KiemTraDuLieuTimKiem()) {
                that.NapDuLieuTimKiem();
            }
        });
        $('#txtDienThoai').change(function () {
            if (that.KiemTraDuLieuTimKiem()) {
                that.NapDuLieuTimKiem();
            }
        });
        if ($('#txtNgaySinh').val() && !that.IsRedirect) {
            that.KiemTraTuoi($('#txtNgaySinh').val());
        }
        if (that.KiemTraDuLieuTimKiem()) {
            that.NapDuLieuTimKiem();
        }

        $('#txtTenTruong').autocomplete({
            source: function (request, response) {
                var formData = new FormData();
                formData.append("hanhDong", "layDanhSachStation");
                formData.append("Term", that.ExtractLast(request.term));
                formData.append("TypeId", 1);
                $.ajax({
                    url: "ActionHandler.ashx",
                    type: "POST",
                    dataTye: "json",
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        var data = $.parseJSON(result);
                        if (data.st != 0) {
                            response($.map(data.msg, function (item) {
                                return {
                                    label: item.Name,
                                    value: item.Name,
                                    id: item.PK_OriginalStationID
                                };
                            }));
                        }
                    },
                    error: function (xhr, status) {
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                $('#hdIdTruong').val(ui.item.id);
            }
        });
        $('#txtTenKCN').autocomplete({
            source: function (request, response) {
                var formData = new FormData();
                formData.append("hanhDong", "layDanhSachStation");
                formData.append("Term", that.ExtractLast(request.term));
                formData.append("TypeId", 2);
                $.ajax({
                    url: "ActionHandler.ashx",
                    type: "POST",
                    dataTye: "json",
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        var data = $.parseJSON(result);
                        if (data.st !== 0) {
                            response($.map(data.msg, function (item) {
                                return {
                                    label: item.Name,
                                    value: item.Name,
                                    id: item.PK_OriginalStationID
                                };
                            }));
                        }
                    },
                    error: function (xhr, status) {
                    }
                });
            },
            minLength: 2,
            select: function (event, ui) {
                $('#hdIdKCN').val(ui.item.id);
            }
        });

        $('#txtTenTruong').change(function() {
            if ($.trim($(this).val()) === '') {
                $('#hdIdTruong').val("");
            }
        });
        $('#txtTenKCN').change(function () {
            if ($.trim($(this).val()) === '') {
                $('#hdIdKCN').val("");
            }
        });

        // #region Bỏ
        //$('#txtTenDiaChi').autocomplete({
        //    source: function (request, response) {
        //        var formData = new FormData();
        //        formData.append("hanhDong", "layDanhSachStation");
        //        formData.append("Term", that.ExtractLast(request.term));
        //        formData.append("TypeId", 3);
        //        $.ajax({
        //            url: "ActionHandler.ashx",
        //            type: "POST",
        //            dataTye: "json",
        //            data: formData,
        //            contentType: false,
        //            processData: false,
        //            success: function (result) {
        //                var data = $.parseJSON(result);
        //                if (data.st != 0) {
        //                    response($.map(data.msg, function (item) {
        //                        return {
        //                            label: item.Name,
        //                            value: item.Name,
        //                            id: item.PK_OriginalStationID
        //                        };
        //                    }));
        //                }
        //            },
        //            error: function (xhr, status) {
        //            }
        //        });
        //    },
        //    minLength: 2,
        //    select: function (event, ui) {
        //        console.log("Bạn vừa chọn: " + ui.item.value + " có mã " + ui.item.id);
        //        $('#hdIdDiaChi').val(ui.item.id);
        //    }
        //});
        // #endregion

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) || that.TuyChon.isM === true) {
            $('.selectpicker').selectpicker({ mobile: true });
        }
    } catch (ex) {
        console.log(ex);
    }
};
DangKyVe.prototype.SplitString = function (val) {
    return val.split(/,\s*/);
};
DangKyVe.prototype.ExtractLast = function (term) {
    var that = this;
    return that.SplitString(term).pop();
};

DangKyVe.prototype.KiemTraTuoi = function (ngaySinh) {
    var that = this;
    var thangTuoi = that.TinhTuoiTheoThang(ngaySinh);
    if (thangTuoi < 72) {
        if ($('#slLoaiDoiTuong').val() === '2' /*|| $('#slLoaiDoiTuong').val() === '3' edit 28082019 sonkt 2926 */) {
            $.alert({
                title: 'Thông báo!',
                escapeKey: 'OK',
                columnClass: 'col-md-6 col-md-offset-3',
                closeIcon: true,
                content: 'Khách hàng thuộc đối tượng miễn phí đi xe buýt. Vui lòng chọn lại loại đối tượng để được làm thẻ ưu tiên.',
                buttons: {
                    OK: {
                        text: 'Tôi đã hiểu',
                        btnClass: 'btn-danger'
                    }
                }
            });
        } else {
            $.alert({
                title: 'Thông báo!',
                escapeKey: 'OK',
                columnClass: 'col-md-6 col-md-offset-3',
                closeIcon: true,
                content: 'Khách hàng thuộc đối tượng miễn phí đi xe buýt',
                buttons: {
                    OK: {
                        text: 'Tôi đã hiểu',
                        btnClass: 'btn-primary'
                    }
                }
            });
        }
    }
    else if (thangTuoi <= 216) {
        if (($('#slLoaiDoiTuong').val() !== '1' && $('#rdDoiTuong_UuTien').is(':checked')) || $('#rdDoiTuong_BinhThuong').is(':checked')) {
            $.alert({
                title: 'Thông báo!',
                escapeKey: 'OK',
                columnClass: 'col-md-6 col-md-offset-3',
                closeIcon: true,
                content:
                    'Khách hàng thuộc đối tượng Ưu tiên: <strong> Học sinh – Sinh viên </strong> . Hãy chọn lại mục <strong> Đối tượng</strong> để được làm thẻ ưu tiên.',
                buttons: {
                    OK: {
                        text: 'Tôi đã hiểu',
                        btnClass: 'btn-danger'
                    }
                }
            });
        }
    }
    //else if (thangTuoi < 720) {

    //}
    //else {
    //    if (($('#slLoaiDoiTuong').val() !== '3' && $('#rdDoiTuong_UuTien').is(':checked')) || $('#rdDoiTuong_BinhThuong').is(':checked')) {
    //        $.alert({
    //            title: 'Thông báo!',
    //            escapeKey: 'OK',
    //            columnClass: 'col-md-6 col-md-offset-3',
    //            closeIcon: true,
    //            content:
    //                'Khách hàng thuộc đối tượng Ưu tiên: <strong>Người cao tuổi</strong> . Hãy chọn lại mục <strong> Đối tượng</strong> để được làm thẻ ưu tiên.',
    //            buttons: {
    //                OK: {
    //                    text: 'Tôi đã hiểu',
    //                    btnClass: 'btn-danger'
    //                }
    //            }
    //        });
    //    }
    //}
}

/**
 * Xóa báo lỗi của selectbox sau khi đã điền đủ
 * @param {any} obj
 */
DangKyVe.prototype.XoaBaoLoiSelect = function (obj) {
    var parent = obj.parents(".form-group");
    parent.removeClass("has-error");
    parent.children('em').last().remove();
};
/**
 * Nạp dữ liệu tìm kiếm
 */
DangKyVe.prototype.NapDuLieuTimKiem = function () {
    var that = this;
    var hoTen = $('#txtHoTen').val();
    var dienThoai = $('#txtDienThoai').val();
    var ngaySinh = $('#txtNgaySinh').val();

    //! Tạo formData để đẩy lên server
    var formData = new FormData();
    formData.append("hanhDong", "timHoSoDeSua");
    formData.append("FullName", hoTen);
    formData.append("BirthDay", ngaySinh);
    formData.append("PhoneNumber", dienThoai);
    $.ajax({
        url: "ActionHandler.ashx",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (result) {
            var ketQua = $.parseJSON(result);
            switch (ketQua.st) {
                //! Tìm thấy dữ liệu thì kiểm tra tiếp xem có sửa đc ko
                case 1:
                    //! Mở form nếu nó bị đóng bằng 1 cách nào đó
                    that.KhoaForm(false);
                    var msg = "";
                    var createdDate = new moment(ketQua.ticket.CreatedDate);
                    switch (ketQua.ticket.State) {
                        //! Có thể sửa
                        case 1:
                            $.alert({
                                title: 'Thông báo!',
                                columnClass: 'col-md-6 col-md-offset-3',
                                content: 'Đơn của quý khách đăng ký ngày <span class="text-red">' + createdDate.format("DD/MM/YYYY") + '</span> đang ở trạng thái <span class="text-red"> ' + that.TrangThaiVe[ketQua.ticket.State] + '</span>.<br/>Quý khách có thể <span class="text-red">Cập nhật hồ sơ đăng ký</span> hoặc <span class="text-red">Hủy hồ sơ đăng ký</span>.<br/>Hoặc vui lòng liên hệ tới số hotline <span class="text-red">1900 1296</span> để được hỗ trợ',
                                buttons: {
                                    OK: {
                                        text: 'Đồng ý',
                                        btnClass: 'btn-default',
                                        action: function () {
                                            that.KhoaFormDangKy();
                                            $('#frmDangKyThe').attr('data-action', 'edit');
                                            $('#btnDangKy').text('Cập nhật hồ sơ');
                                            $('#btnHuyHoSo').removeClass('hidden');
                                            that.DienDuLieuVaoForm(ketQua.ticket);
                                        }
                                    }
                                }
                            });
                            break;
                        case 2:
                            $.alert({
                                title: 'Thông báo!',
                                columnClass: 'col-md-6 col-md-offset-3',
                                content: '<p>Đơn của quý khách đăng ký ngày <span class="text-red">' + createdDate.format("DD/MM/YYYY") + '</span> đang ở trạng thái <span class="text-red"> ' + that.TrangThaiVe[ketQua.ticket.State] + '</span>. Quý khách vui lòng <span class="text-red">Cập nhật hồ sơ</span>  theo các nội dung sau:</p><p class="text-red" style="padding-left:30px;"> -' + ketQua.ticket.NoteForCustomer + '</p> <p>Hoặc lựa chọn <span class="text-red">Hủy hồ sơ</span> đã đăng ký.</p>Hoặc vui lòng liên hệ tới số hotline <span class="text-red">1900 1296</span> để được hỗ trợ',
                                buttons: {
                                    OK: {
                                        text: 'Đồng ý',
                                        btnClass: 'btn-default',
                                        action: function () {
                                            that.KhoaFormDangKy();
                                            $('#frmDangKyThe').attr('data-action', 'edit');
                                            $('#btnDangKy').text('Cập nhật hồ sơ');
                                            $('#btnHuyHoSo').removeClass('hidden');
                                            that.DienDuLieuVaoForm(ketQua.ticket);
                                        }
                                    }
                                }
                            });
                            break;
                        //! Cho thêm mới bình thường nói cách khác là giống sửa, nhưng Insert record khác vào db. Bỏ check tháng mua tem
                        case 6:
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                        case 16:
                            that.DienDuLieuVaoForm(ketQua.ticket);
                            that.KhoaFormDangKy();
                            $('#frmDangKyThe').attr('data-action', 'add');
                            $('#btnDangKy').text('Đăng ký');
                            $('#btnHuyHoSo').addClass('hidden');
                            break;
                        //! Đơn hàng hủy xét số ngày + 35
                        //case 15:
                        //var ngayHienTai = new Date();
                        //var ngayToiDa = eval(that.TuyChon.ngayNhanTheMuonNhat);//! Trước fix cứng là 35
                        //var ngayBoSung = new Date(new Date(ketQua.ticket.UpdateDateCustomer).getTime() + ngayToiDa * 24 * 60 * 60 * 1000);
                        //if (ngayBoSung >= ngayHienTai) {
                        //    msg += 'Quý khách vui lòng liên hệ số hotline <span class="text-red">1900 1296</span> để được hỗ trợ';
                        //    $.alert({
                        //        title: 'Thông báo!',
                        //        escapeKey: 'OK',
                        //        columnClass: 'col-md-6 col-md-offset-3',
                        //        closeIcon: true,
                        //        content: msg,
                        //        buttons: {
                        //            OK: {
                        //                text: 'Tôi đã hiểu',
                        //                btnClass: 'btn-danger',
                        //                action: function () {
                        //                    $('#btnNhapLai').trigger('click');
                        //                }
                        //            }
                        //        }
                        //    });
                        //    $('#frmDangKyThe').attr('data-action', 'block');
                        //    that.KhoaForm(true, 'frmDangKyThe');
                        //}
                        ////! Đăng ký mới
                        //else {
                        //    that.DienDuLieuVaoForm(ketQua.ticket);
                        //    $('#frmDangKyThe').attr('data-action', 'add');
                        //    $('#btnDangKy').text('Đăng ký');
                        //    $('#btnHuyHoSo').addClass('hidden');
                        //}
                        //break;
                        //! Các trường hợp khác không được sửa
                        case 3:
                        case 4:
                        case 5:
                        case 7:
                        case 8:
                        default:
                            $('#frmDangKyThe').attr('data-action', 'block');
                            if (that.TuyChon.isM == true) {
                                msg = 'Đơn của quý khách đăng ký ngày <span class="text-red">' + createdDate.format("DD/MM/YYYY") + '</span> đang ở trạng thái <span class="text-red"> ' + that.TrangThaiVe[ketQua.ticket.State] + '</span>.<br/>Quý khách vui lòng vào mục Tra cứu để theo dõi trạng thái đơn hàng.<br/>Hoặc vui lòng liên hệ tới số hotline <span class="text-red">1900 1296</span> để được hỗ trợ';
                            }
                            else {
                                msg = 'Đơn của quý khách đăng ký ngày <span class="text-red">' + createdDate.format("DD/MM/YYYY") + '</span> đang ở trạng thái <span class="text-red"> ' + that.TrangThaiVe[ketQua.ticket.State] + '</span>.<br/>Quý khách vui lòng vào mục  <a href="TraCuu.aspx" class="text-bold text-uppercase">Tra cứu</a> để theo dõi trạng thái đơn hàng.<br/>Hoặc vui lòng liên hệ tới số hotline <span class="text-red">1900 1296</span> để được hỗ trợ';
                            }
                            $.alert({
                                title: 'Thông báo!',
                                escapeKey: 'OK',
                                columnClass: 'col-md-6 col-md-offset-3',
                                closeIcon: true,
                                content: msg,
                                buttons: {
                                    OK: {
                                        text: 'Tôi đã hiểu',
                                        btnClass: 'btn-danger'
                                    }
                                }
                            });
                            that.DienDuLieuVaoForm(ketQua.ticket);
                            that.KhoaForm(true, 'frmDangKyThe');
                            break;
                    }
                    break;
                //! Không tìm thấy dữ liệu, để đăng ký bình thường
                case 0:
                    $('#frmDangKyThe').attr('data-action', 'add');
                    $('#btnDangKy').text('Đăng ký');
                    $('#btnHuyHoSo').addClass('hidden');
                    break;
            }
        },
        error: function (xhr, status) {
            console.log("Có lỗi xảy ra: " + status);
        }
    });
};
/**
 * Khóa toàn bộ form
 * @param {any} yes
 * @param {any} formId
 */
DangKyVe.prototype.KhoaForm = function (yes, formId) {
    $('#' + formId + ' input').attr('disabled', yes);
    $('#' + formId + ' button').attr('disabled', yes);
    $('#' + formId + ' button[data-toggle="popover"]').removeAttr("disabled");
}

/**
 * Điền dữ liệu vào form
 * @param {any} ticket
 */
DangKyVe.prototype.DienDuLieuVaoForm = function (ticket) {
    var that = this;
    //! Ẩn cái nút nhập lại đi
    $('#btnNhapLai').hide();
    //! Ẩn các preview không cần thiết trước khi hiện nó lên
    $('#prvChonAnhThe').addClass('hidden');
    $('#prvMatTruoc').addClass('hidden');
    $('#prvMatSau').addClass('hidden');
    $('#prvDonDangKy').addClass('hidden');
    $('#prvDonXacNhan').addClass('hidden');
    //! Xét đối tượng ưu tiên
    switch (ticket.TicketType) {
        //! Đối tượng bình thường
        case 0:
            $('#rdDoiTuong_BinhThuong').prop('checked', true);
            $('#rdDoiTuong_UuTien').prop('checked', false);
            $('#rdDoiTuong_BinhThuong').trigger('click');
            //$('#hdIdDiaChi').val(ticket.FK_OriginalStationID);
            $('#slDiaChi').val(ticket.FK_OriginalStationID);
            $('#slDiaChi').selectpicker('refresh');
            that.ControlHocSinh(false);
            that.ControlCongNhan(false);
            //that.ControlNguoiCaoTuoi(false);
            break;
        //! Học sinh, sinh viên
        case 1:
            $('#rdDoiTuong_BinhThuong').prop('checked', false);
            $('#rdDoiTuong_UuTien').prop('checked', true);
            $('#rdDoiTuong_UuTien').trigger('click');
            var year = moment(ticket.SchoolYear).year();
            $('#slNienKhoa').val(year);
            $('#slNienKhoa').selectpicker('refresh');
            $('#txtTenTruong').val(ticket.OriginalStationIDName);
            $('#hdIdTruong').val(ticket.FK_OriginalStationID);
            //that.XoaBaoLoiSelect($('#slTruongHoc'));
            $('#txtDonDangKy').val(ticket.AppImg);
            $('#prvDonDangKy').removeClass('hidden');
            that.LayDuLieuAnh(that.BaseImageUrl + ticket.AppImg,
                function (data) {
                    $('#txtDonDangKy').attr('data-content', data);
                });

            that.ControlHocSinh(true);
            that.ControlCongNhan(false);
           //that.ControlNguoiCaoTuoi(false);
            break;
        //! Công nhân các khu công nghiệp
        case 2:
            $('#rdDoiTuong_BinhThuong').prop('checked', false);
            $('#rdDoiTuong_UuTien').prop('checked', true);
            $('#rdDoiTuong_UuTien').trigger('click');
            $('#txtTenKCN').val(ticket.OriginalStationIDName);
            $('#hdIdKCN').val(ticket.FK_OriginalStationID);
            //that.XoaBaoLoiSelect($('#slKhuCongNghiep'));
            $('#txtDonXacNhan').val(ticket.AppImg);
            $('#prvDonXacNhan').removeClass('hidden');
            that.LayDuLieuAnh(that.BaseImageUrl + ticket.AppImg,
                function (data) {
                    $('#txtDonXacNhan').attr('data-content', data);
                });

            that.ControlHocSinh(false);
            that.ControlCongNhan(true);
            //that.ControlNguoiCaoTuoi(false);
            break;
        //! Người cao tuổi
        //case 3:
        //    $('#rdDoiTuong_BinhThuong').prop('checked', false);
        //    $('#rdDoiTuong_UuTien').prop('checked', true);
        //    $('#rdDoiTuong_UuTien').trigger('click');
        //    //$('#hdIdDiaChi').val(ticket.FK_OriginalStationID);
        //    $('#slDiaChi').val(ticket.FK_OriginalStationID);
        //    $('#slDiaChi').selectpicker('refresh');
        //    that.ControlHocSinh(false);
        //    that.ControlCongNhan(false);
        //    that.ControlNguoiCaoTuoi(true);
        //    break;
    }
    if (ticket.TicketType != 0) {
        $('#slLoaiDoiTuong').val(ticket.TicketType);
        $('#slLoaiDoiTuong').selectpicker('refresh');
        //that.XoaBaoLoiSelect($('#slLoaiDoiTuong'));
        $('#slLoaiDoiTuong').trigger('change');
    }
    $('#txtAnhCMNDMatSau').val(ticket.BackImg);
    $('#prvMatSau').removeClass('hidden');
    that.LayDuLieuAnh(that.BaseImageUrl + ticket.BackImg,
        function (data) {
            $('#txtAnhCMNDMatSau').attr('data-content', data);
        });

    $('#txtAnhCMNDMatTruoc').val(ticket.FrontImg);
    $('#prvMatTruoc').removeClass('hidden');
    that.LayDuLieuAnh(that.BaseImageUrl + ticket.FrontImg,
        function (data) {
            $('#txtAnhCMNDMatTruoc').attr('data-content', data);
        });

    //! Một tuyến
    if (ticket.FleetType === 1) {
        $('#rdLoaiThe_MotTuyen').prop('checked', true);
        $('#rdLoaiThe_LienTuyen').prop('checked', false);
        $('#rdLoaiThe_MotTuyen').trigger('click');
        $('#slTuyen').val(ticket.RegisterFleet);
        $('#slTuyen').selectpicker('refresh');
        //that.XoaBaoLoiSelect($('#slTuyen'));
    }
    //! Liên tuyến
    else {
        $('#rdLoaiThe_MotTuyen').prop('checked', false);
        $('#rdLoaiThe_LienTuyen').prop('checked', true);
        $('#rdLoaiThe_LienTuyen').trigger('click');
    }
    //! Đăng ký nhận tại nhà hay không
    if (ticket.IsShipHome) {
        $('#chkNhanTaiNha').prop('checked', true);
        $('div[data-control="coTaiNha"]').removeClass("hidden");
        $('div[data-control="khongTaiNha"]').addClass("hidden");
        $('#slQuan').val(ticket.ReceiverDistrictId);
        $('#slQuan').selectpicker('refresh');
        //that.XoaBaoLoiSelect($('#slQuan'));
        $('#txtDiaChiNhanThe').val(ticket.ReceiverAddress);
        var thangMuaTem = ticket.RegistryStampMonths.split(',');
        //! Kiểm tra tháng thứ 1 xem có trong tháng đăng ký mua không
        var thangThu_1 = $('#chkThangThu1').val();
        if (thangMuaTem.indexOf(thangThu_1) >= 0) {
            $('#chkThangThu1').prop('checked', true);
            $('#chkThangThu1').trigger('change');
        }

        //! Kiểm tra tháng thứ 1 xem có trong tháng đăng ký mua không
        var thangThu_2 = $('#chkThangThu2').val();
        if (thangMuaTem.indexOf(thangThu_2) >= 0) {
            $('#chkThangThu2').prop('checked', true);
            $('#chkThangThu2').trigger('change');
        }
        //! Kiểm tra tháng thứ 1 xem có trong tháng đăng ký mua không
        var thangThu_3 = $('#chkThangThu3').val();
        if (thangMuaTem.indexOf(thangThu_3) >= 0) {
            $('#chkThangThu3').prop('checked', true);
            $('#chkThangThu3').trigger('change');
        }

        that.TinhPhiTem();
    } else {
        $('#chkNhanTaiNha').prop('checked', false);
        $('div[data-control="coTaiNha"]').addClass("hidden");
        $('div[data-control="khongTaiNha"]').removeClass("hidden");
        $('#slNoiNhan').val(ticket.ReceiverTicketPlace);
        $('#slNoiNhan').selectpicker('refresh');

    }

    //! Dữ liệu luôn phải điền
    $('#txtAnhLamThe').val(ticket.CardImg);
    $('#prvChonAnhThe').removeClass('hidden');
    $('#txtOldState').val(ticket.State);
    $('#txtCreatedDate').val(moment(ticket.CreatedDate).format("DD/MM/YYYY"));
    that.LayDuLieuAnh(that.BaseImageUrl + ticket.CardImg,
        function (data) {
            $('#txtAnhLamThe').attr('data-content', data);
        });
    $('#dtpNgayNhanThe').data('DateTimePicker').date(moment(ticket.ReceiverDate));
    //! Key của ticket cần sửa
    $('#txtTicketId').val(ticket.MaTicket);
    //! Chỉnh lại các thông báo lỗi nếu có
    $('#frmDangKyThe').valid();
};
/**
 * Khóa 3 control tìm kiếm khi tìm thấy
 */
DangKyVe.prototype.KhoaFormDangKy = function () {
    $('#txtDienThoai').prop('readonly', 'readonly');
    $('#txtHoTen').prop('readonly', 'readonly');
    $('#dtpNgaySinh').data('DateTimePicker').disable();
};
/**
 * Khóa 3 control tìm kiếm khi tìm thấy
 */
DangKyVe.prototype.MoFormDangKy = function () {
    $('#txtDienThoai').attr('readonly', false);
    $('#txtHoTen').attr('readonly', false);
    $('#dtpNgaySinh').data('DateTimePicker').enable();
};
/**
 * Kiểm tra dữ liệu tìm kiếm
 */
DangKyVe.prototype.KiemTraDuLieuTimKiem = function () {
    return $("#txtHoTen").val() !== '' && $("#txtDienThoai").val() !== '' && $("#txtNgaySinh").val() !== '';
};
/**
 * tính toán khoảng cách 2 giá trị ngày theo tháng
 * @param {any} d1
 * @param {any} d2
 */
DangKyVe.prototype.DateDiifInMonths = function (d1, d2) {
    var d1Y = d1.getFullYear();
    var d2Y = d2.getFullYear();
    var d1M = d1.getMonth();
    var d2M = d2.getMonth();

    return (d2M + 12 * d2Y) - (d1M + 12 * d1Y);
}
/**
 * Đăng ký sự kiện nút Thêm
 */
DangKyVe.prototype.DangKyNutThem = function () {
    var that = this;
    if ($("#frmDangKyThe")) {
        //! Trường hợp đăng ký mới

        //$.validator.addMethod("nguoiCaoTuoi", function (value, element) {
        //    var loaiDoiTuong = $("#slLoaiDoiTuong").val();
        //    var tuoi = that.TinhTuoiTheoThang(value);
        //    if (loaiDoiTuong === "3" && !(tuoi > 719)) {
        //        return false;
        //    } else {
        //        return true;
        //    }
        //}, "Quý khách chưa đủ tuổi ưu tiên người cao tuổi. Xin quí khách vui lòng chọn lại đối tượng ưu đăng ký vé");

        $.validator.addMethod("noempty", function (value, element) {
            var v = that.XoaKhoangTrang(value);
            if (v==="") {
                return false;
            } else {
                return true;
            }
        }, "");
        $.validator.addMethod("truonghoc", function (value, element) {
            var v = that.XoaKhoangTrang(value);
            if (v === "" && $("#slLoaiDoiTuong").val() === "1") {
                return false;
            } else {
                return true;
            }
        }, "Vui lòng nhập trường học");
        $.validator.addMethod("khucongnghiep", function (value, element) {
            var v = that.XoaKhoangTrang(value);
            if (v === "" && $("#slLoaiDoiTuong").val() === "2") {
                return false;
            } else {
                return true;
            }
        }, "Vui lòng nhập khu công nghiệp");

        //! Validate cho niên khóa
        $.validator.addMethod("nienKhoaHopLe", function (value, element) {
            if ($("#slLoaiDoiTuong").val() === "1" && ($.trim(value) === "" || $.trim(value) === "__/__/____")) {
                $.validator.messages["nienKhoaHopLe"] = "Vui lòng nhập vào niên khóa";
                return false;
            }
            var date = new Date();
            var currentYear = date.getFullYear() + "";
            var currentMonth = date.getMonth();
            if (value === currentYear && currentMonth > 8) {
                $.validator.messages["nienKhoaHopLe"] = "Bạn đã hết hạn ưu tiên. Bạn nên chọn lại loại thẻ Bình Thường";
                return false;
            }
            return true;
        }, 'Vui lòng nhập niên khóa phù hợp');

        $("#frmDangKyThe").validate({
            rules: {
                txtHoTen: {
                    noempty:true,
                    maxlength: 50,
                    minlength: 4
                },
                txtNgaySinh: {
                    noempty: true
                    //nguoiCaoTuoi: true
                },
                txtDienThoai: {
                    noempty: true,
                    number: true,
                    maxlength: 12
                },
                slDiaChi: {
                    required: {
                        depends: function () {
                            return $("#slLoaiDoiTuong").val() === "0";
                        }
                    }
                },
                slLoaiDoiTuong: {
                    min: {
                        depends: function () {
                            return $("#rdDoiTuong_UuTien").is(":checked") && $("#slLoaiDoiTuong").val() === "0";
                        }
                    }
                },
                txtTenTruong: {
                    truonghoc: true
                },
                txtTenKCN: {
                    khucongnghiep: true
                },
                slNienKhoa: {
                    nienKhoaHopLe:true
                },
                txtAnhLamThe: {
                    noempty: true
                },
                txtAnhCMNDMatTruoc: {
                    required: {
                        depends: function () {
                            return $("#slLoaiDoiTuong").val() !== "0";
                        }
                    }
                },
                txtAnhCMNDMatSau: {
                    required: {
                        depends: function () {
                            return $("#slLoaiDoiTuong").val() !== "0";
                        }
                    }
                },
                txtDonDangKy: {
                    required: {
                        depends: function () {
                            return $("#slLoaiDoiTuong").val() === "1";
                        }
                    }
                },
                txtDonXacNhan: {
                    required: {
                        depends: function () {
                            return $("#slLoaiDoiTuong").val() === "2";
                        }
                    }
                },
                slTuyen: {
                    required: {
                        depends: function () {
                            return $("#rdLoaiThe_MotTuyen").is(":checked") && $('#slTuyen').val() === '';
                        }
                    }
                },
                slNoiNhan: {
                    required: {
                        depends: function () {
                            return !$("#chkNhanTaiNha").is(":checked") && $('#slNoiNhan').val() === '';
                        }
                    }
                },
                slQuan: {
                    required: {
                        depends: function () {
                            return $("#chkNhanTaiNha").is(":checked");
                        }
                    }
                },
                txtDiaChiNhanThe: {
                    required: {
                        depends: function () {
                            return $("#chkNhanTaiNha").is(":checked");
                        }
                    },
                    maxlength: 150
                },
                txtNgayNhanThe: {
                    noempty: true
                },
                txtCaptcha: {
                    noempty: true
                }
            },
            messages: {
                txtHoTen: {
                    noempty: "Bạn phải nhập đầy đủ họ tên",
                    maxlength: "Họ tên không quá 50 ký tự",
                    minlength: "Họ tên tối thiểu 4 ký tự"
                },
                txtNgaySinh: {
                    noempty: "Ngày sinh không để trống"
                },
                txtDienThoai: {
                    noempty: "Điện thoại không để trống",
                    number: "Điện thoại chỉ chứa ký tự số",
                    maxlength: "Điện thoại không quá 12 ký tự"
                },
                slDiaChi: {
                    required: "Vui lòng chọn địa chỉ"
                },
                slLoaiDoiTuong: {
                    min: "Vui lòng chọn đối tượng ưu tiên"
                },
                txtTenTruong: {
                    required: "Vui lòng chọn trường"
                },
                txtTenKCN: {
                    required: "Vui lòng chọn khu công nghiệp"
                },
                slNienKhoa: {
                    required: "Vui lòng chọn niên khóa"
                },
                txtAnhLamThe: {
                    noempty: "Vui lòng chọn ảnh"
                },
                txtAnhCMNDMatTruoc: {
                    required: "Ảnh thẻ mặt trước không để trống"
                },
                txtAnhCMNDMatSau: {
                    required: "Ảnh thẻ mặt sau không để trống"
                },
                txtDonDangKy: {
                    required: "Ảnh đơn đăng ký không để trống"
                },
                txtDonXacNhan: {
                    required: "Ảnh đơn xác nhận không để trống"
                },
                slTuyen: {
                    required: "Vui lòng chọn tuyến"
                },
                slNoiNhan: {
                    required: "Vui lòng chọn nơi nhận"
                },
                slQuan: {
                    required: "Vui lòng chọn quận"
                },
                txtDiaChiNhanThe: {
                    required: "Địa chỉ nhận thẻ không để trống",
                    maxlength: "Địa chỉ không vượt quá 150 ký tự"
                },
                txtNgayNhanThe: {
                    noempty: "Ngày nhận thẻ không để trống"
                },
                txtCaptcha: {
                    noempty: "Vui lòng nhập mã xác thực"
                }
            },
            errorElement: "em",
            highlight: function (element, errorClass, validClass) {
                $(element).parents(".form-group").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".form-group").addClass("has-success").removeClass("has-error");
            },
            errorPlacement: function (error, element) {
                //error.insertAfter(element).wrap('<li/>');
                error.addClass("help-block col-sm-8 col-xs-12 col-md-offset-4");
                error.insertAfter(element.parents('.form-group').children().last());
            },
            success: function (label, element) {
                label.parent().removeClass('error');
                label.remove();
            },
            submitHandler: function (form) {
                var hanhDong = form.getAttribute("data-action");
                //! Gọi hàm lấy giá trị từ form vào đối tượng
                that.LayGiaTriForm();
                var ngayKhongChe = that.TinhNgayNhanThe(that.TuyChon);
                var ngayNhanThe = moment(that.NgayNhanThe, "DD-MM-YYYY");
                var thu = ngayNhanThe.day() + '';
                if (ngayKhongChe.ngayNghi.indexOf(thu) >= 0) {
                    $.confirm({
                        title: 'Thông báo!',
                        columnClass: 'col-md-6 col-md-offset-3',
                        content: 'Ngày nhận thẻ trùng vào ngày nghỉ. Vui lòng chọn lại.',
                        buttons: {
                            confirm: {
                                text: 'Tôi đã hiểu',
                                btnClass: 'btn-danger'
                            }
                        }
                    });
                }
                else if (that.NhanTaiNha && that.ThangDangKyMua.length <= 0) {
                    $.confirm({
                        title: 'Thông báo!',
                        columnClass: 'col-md-6 col-md-offset-3',
                        content: 'Bạn phải chọn ít nhất 1 tháng đăng ký mua tem.',
                        buttons: {
                            confirm: {
                                text: 'Tôi đã hiểu',
                                btnClass: 'btn-danger'
                            }
                        }
                    });
                }
                else {
                    debugger;
                    //! Hiển thị cái quay tròn tròn
                    $('#fsModal').modal({
                        keyboard: false
                    });
                    //! Trường hợp thêm mới
                    if (hanhDong === 'add') {
                        //! Tạo formData để đẩy lên server
                        var formData = that.MakeFormData(form, "dangKyVe");
                        if (formData) {
                            $.ajax({
                                url: "ActionHandler.ashx",
                                type: "POST",
                                data: formData,
                                contentType: false,
                                processData: false,
                                success: function (result) {
                                    var ketQua = $.parseJSON(result);
                                    //Đóng cái xoay xoay
                                    $('#fsModal').modal('hide');
                                    //grecaptcha.reset(); Làm sau
                                    $('#imgCatchaValidate').attr('src',
                                        '/TicketRegister/captchaImg.aspx?ver=' + that.GetRandomText(5));
                                    $("#txtCaptcha").val("");
                                    switch (ketQua.st) {
                                        case 1:
                                            //! Thành công
                                            var hoanThanh = $("#dangKyHoanThanh").html();
                                            $("#dangKy").html(hoanThanh);
                                            break;
                                        case 0:
                                            $.alert({
                                                title: 'Thông báo!',
                                                escapeKey: 'OK',
                                                closeIcon: true,
                                                columnClass: 'col-md-6 col-md-offset-3',
                                                content:
                                                    'Mã xác thực không chính xác! Quý khách vui lòng kiểm tra lại',
                                                buttons: {
                                                    OK: {
                                                        text: 'Tôi đã hiểu',
                                                        btnClass: 'btn-danger'
                                                    }
                                                }
                                            });
                                            break;
                                        case -1:
                                            //! Lỗi xác thực dữ liệu 
                                            var errorElement = $("#" + ketQua.fi);
                                            errorElement.parents(".form-group").addClass("has-error")
                                                .removeClass("has-success");
                                            var err = document.createElement('em');
                                            err.className = "help-block col-sm-8 col-xs-12 col-md-offset-4";
                                            err.appendTo(errorElement.parent());
                                            break;
                                        case -2:
                                            //! Lỗi khi thêm dữ liệu vào database phía server
                                            $.alert({
                                                title: 'Thông báo!',
                                                escapeKey: 'OK',
                                                closeIcon: true,
                                                columnClass: 'col-md-6 col-md-offset-3',
                                                content:
                                                    'Có lỗi khi thực hiện đăng ký vé của bạn. Chúng tôi rất tiếc vì sự bất tiện này. Vui lòng quay lại sau.',
                                                buttons: {
                                                    OK: {
                                                        text: 'Tôi đã hiểu',
                                                        btnClass: 'btn-danger'
                                                    }
                                                }
                                            });
                                            break;
                                        case -3:
                                            //! Đơn đăng ký đã tồn tại.
                                            var msg =
                                                'Đơn đăng ký của bạn đã tồn tại trong hệ thống. Quý khách vui lòng vào trang';
                                            msg +=
                                                '<span class="linkToTab text-bold text-uppercase text-underline text-primary"> Tra cứu vé tháng</span>.<br/>Nhập thông tin: Họ tên, ngày sinh, điện thoại để tra cứu tình trạng đơn đăng ký.<br/>';
                                            msg +=
                                                'Hoặc vui lòng liên hệ số hotline <span class="text-red">1900 1296</span> để được hỗ trợ';
                                            $.alert({
                                                title: 'Thông báo!',
                                                escapeKey: 'OK',
                                                closeIcon: true,
                                                columnClass: 'col-md-6 col-md-offset-3',
                                                content: msg,
                                                buttons: {
                                                    OK: {
                                                        text: 'Tôi đã hiểu',
                                                        btnClass: 'btn-danger'
                                                    }
                                                }
                                            });
                                            break;
                                        case -4:
                                            //! Có lỗi khi lưu ảnh
                                            $.alert({
                                                title: 'Thông báo!',
                                                escapeKey: 'OK',
                                                closeIcon: true,
                                                columnClass: 'col-md-6 col-md-offset-3',
                                                content: ketQua.msg,
                                                buttons: {
                                                    OK: {
                                                        text: 'Tôi đã hiểu',
                                                        btnClass: 'btn-danger'
                                                    }
                                                }
                                            });
                                            break;
                                    }
                                },
                                error: function (xhr, status) {
                                    //Đóng cái xoay xoay
                                    $('#fsModal').modal('hide');
                                    console.log("Có lỗi xảy ra: " + status);
                                }
                            });
                        } else {
                            //Đóng cái xoay xoay
                            $('#fsModal').modal('hide');
                            console.log('Lỗi cmnr');
                        }
                    }
                    //! Trường hợp sửa
                    else if (hanhDong === 'edit') {
                        //! Tạo formData để đẩy lên server
                        var formData = that.MakeFormData(form, "capNhatVe");
                        if (formData) {

                            $.ajax({
                                url: "ActionHandler.ashx",
                                type: "POST",
                                data: formData,
                                contentType: false,
                                processData: false,
                                success: function (result) {
                                    var ketQua = $.parseJSON(result);
                                    //Đóng cái xoay xoay
                                    $('#fsModal').modal('hide');
                                    //grecaptcha.reset(); -- Làm sau
                                    $('#imgCatchaValidate').attr('src',
                                        '/TicketRegister/captchaImg.aspx?ver=' + that.GetRandomText(5));
                                    $("#txtCaptcha").val("");
                                    switch (ketQua.st) {
                                        case 1:
                                            //! Thành công
                                            var hoanThanh = $("#dangKyHoanThanh").html();
                                            $("#dangKy").html(hoanThanh);
                                            $('.noticeTitle').text('Hồ sơ đăng ký của quý khách đã được cập nhật và gửi đến Trung Tâm Vé - Transerco thành công');
                                            break;
                                        case 0:
                                            $.alert({
                                                title: 'Thông báo!',
                                                escapeKey: 'OK',
                                                closeIcon: true,
                                                columnClass: 'col-md-6 col-md-offset-3',
                                                content:
                                                    'Mã xác thực không chính xác! Quý khách vui lòng kiểm tra lại',
                                                buttons: {
                                                    OK: {
                                                        text: 'Tôi đã hiểu',
                                                        btnClass: 'btn-danger'
                                                    }
                                                }
                                            });
                                            break;
                                        case -1:
                                            //! Lỗi xác thực dữ liệu 
                                            var errorElement = $("#" + ketQua.fi);
                                            errorElement.parents(".form-group").addClass("has-error")
                                                .removeClass("has-success");
                                            var err = document.createElement('em');
                                            err.className = "help-block col-sm-8 col-xs-12 col-md-offset-4";
                                            errorElement.parent().append(err);
                                            break;
                                        case -2:
                                            //! Lỗi khi thêm dữ liệu vào database phía server
                                            //! Xem trạng thái lỗi
                                            if (ketQua.isStateChange !== 0) {
                                                $.alert({
                                                    title: 'Thông báo!',
                                                    escapeKey: 'OK',
                                                    closeIcon: true,
                                                    columnClass: 'col-md-6 col-md-offset-3',
                                                    content: 'Đơn của quý khách đăng ký ngày <span class="text-red">' +
                                                        $('#txtCreatedDate').val() +
                                                        '</span> có thể vừa được chuyển sang trạng thái <span class="text-red"> ' +
                                                        that.TrangThaiVe[ketQua.isStateChange] +
                                                        '</span>.<br/>Quý khách vui lòng liên hệ tới số hotline <span class="text-red">1900 1296</span> để được hỗ trợ',
                                                    buttons: {
                                                        OK: {
                                                            text: 'Tôi đã hiểu',
                                                            btnClass: 'btn-danger'
                                                        }
                                                    }
                                                });
                                                return;
                                            }
                                            $.alert({
                                                title: 'Thông báo!',
                                                escapeKey: 'OK',
                                                closeIcon: true,
                                                columnClass: 'col-md-6 col-md-offset-3',
                                                content:
                                                    'Có lỗi khi thực hiện chỉnh sửa thông tin đăng ký vé của bạn. Chúng tôi rất tiếc vì sự bất tiện này. Vui lòng quay lại sau.',
                                                buttons: {
                                                    OK: {
                                                        text: 'Tôi đã hiểu',
                                                        btnClass: 'btn-danger'
                                                    }
                                                }
                                            });
                                            break;
                                        case -3:
                                            $.alert({
                                                title: 'Thông báo!',
                                                escapeKey: 'OK',
                                                closeIcon: true,
                                                columnClass: 'col-md-6 col-md-offset-3',
                                                content: ketQua.msg,
                                                buttons: {
                                                    OK: {
                                                        text: 'Tôi đã hiểu',
                                                        btnClass: 'btn-danger'
                                                    }
                                                }
                                            });
                                            break;
                                    }
                                },
                                error: function (xhr, status) {
                                    //Đóng cái xoay xoay
                                    $('#fsModal').modal('hide');
                                    console.log("Có lỗi xảy ra: " + status);
                                }
                            });
                        } else {
                            //Đóng cái xoay xoay
                            $('#fsModal').modal('hide');
                            console.log('Lỗi cmnr');
                        }
                    }
                }
            }
        });
    }
};

DangKyVe.prototype.XoaKhoangTrang = function (value) {
    if (value) {
        return value.replace(/\s/g,'');
    }
    else {
        return "";
    }
}

/**
 * Hàm này dùng để lấy giá trị trên form vào thuộc tính của đối tượng
 */
DangKyVe.prototype.LayGiaTriForm = function () {
    var that = this;
    that.DonDangKy = $("#txtDonDangKy").val();
    that.DonXacNhan = $("#txtDonXacNhan").val();
    that.AnhThe = $("#txtAnhLamThe").val();
    that.MatTruoc = $("#txtAnhCMNDMatTruoc").val();
    that.MatSau = $("#txtAnhCMNDMatSau").val();
    that.DuLieuDonDangKy = ($("#txtDonDangKy").attr('data-content')) ? $("#txtDonDangKy").attr('data-content') : "";
    that.DuLieuDonXacNhan = ($("#txtDonXacNhan").attr('data-content')) ? $("#txtDonXacNhan").attr('data-content') : "";
    that.DuLieuAnhThe = ($("#txtAnhLamThe").attr('data-content')) ? $("#txtAnhLamThe").attr('data-content') : "";
    that.DuLieuMatTruoc = ($("#txtAnhCMNDMatTruoc").attr('data-content')) ? $("#txtAnhCMNDMatTruoc").attr('data-content') : "";
    that.DuLieuMatSau = ($("#txtAnhCMNDMatSau").attr('data-content')) ? $("#txtAnhCMNDMatSau").attr('data-content') : "";
    that.DoiTuongUuTien = $("#slLoaiDoiTuong").val();
    that.HoTen = $("#txtHoTen").val();
    that.NgaySinh = $("#txtNgaySinh").val();
    that.DienThoai = $("#txtDienThoai").val();
    that.DiaChi = $("#slDiaChi").val();
    that.TenDiaChi = $("#slDiaChi option:selected").text();
    that.TruongHoc = $("#hdIdTruong").val();
    that.TenTruongHoc = $("#txtTenTruong").val();
    that.NienKhoa = $("#slNienKhoa").val();
    that.KhuCongNghiep = $("#hdIdKCN").val();
    that.TenKhuCongNghiep = $("#txtTenKCN").val();
    that.LoaiThe = $("#rdLoaiThe_MotTuyen").is(":checked") ? 1 : 2;
    that.NhanTaiNha = $("#chkNhanTaiNha").is(":checked") ? true : false;
    that.Tuyen = $("#slTuyen").val();
    that.NoiNhanThe = $("#slNoiNhan").val();
    that.DiaChiNhanThe = $("#txtDiaChiNhanThe").val();
    that.Quan = $("#slQuan").val();
    that.NgayNhanThe = $("#txtNgayNhanThe").val();
    that.MaTicket = $("#txtTicketId") ? $("#txtTicketId").val() : "";
    that.TrangThaiCu = $('#txtOldState') ? $('#txtOldState').val() : 0;
}
/**
 * Hàm control người cao tuổi
 * @param {any} obj
 */
//DangKyVe.prototype.ControlNguoiCaoTuoi = function (obj) {
//    var that = this;

//    if (!obj) {
//        $('#txtAnhCMNDMatTruoc').parents('.form-group').addClass('hidden');
//        $('#txtAnhCMNDMatSau').parents('.form-group').addClass('hidden');
//    }
//    else {
//        $('#txtAnhCMNDMatTruoc').parents('.form-group').removeClass('hidden');
//        $('#txtAnhCMNDMatSau').parents('.form-group').removeClass('hidden');
//        $('label[for="txtAnhCMNDMatSau"]').text("CMND (Mặt sau)");
//        $('label[for="txtAnhCMNDMatTruoc"]').text("CMND (Mặt trước)");
//        $('p[data-sample="txtAnhCMNDMatTruoc"]').attr('data-href', that.TuyChon.mauCmndMatTruoc);
//        $('p[data-sample="txtAnhCMNDMatSau"]').attr('data-href', that.TuyChon.mauCmndMatSau);
//    }
//};

/**
 * Hàm ẩn các control của công nhân
 * @param {any} obj
 */
DangKyVe.prototype.ControlCongNhan = function (obj) {
    var that = this;
    if (!obj) {
        $('div[data-control="congNhan"]').addClass('hidden');
        $('#txtAnhCMNDMatTruoc').parents('.form-group').addClass('hidden');
        $('#txtAnhCMNDMatSau').parents('.form-group').addClass('hidden');
        $('.mauDon').hide();
    }
    else {
        $('div[data-control="congNhan"]').removeClass('hidden');
        $('#txtAnhCMNDMatTruoc').parents('.form-group').removeClass('hidden');
        $('#txtAnhCMNDMatSau').parents('.form-group').removeClass('hidden');
        $('.mauDon').show();

        $('label[for="txtAnhCMNDMatSau"]').text("Thẻ CN (Mặt sau)");
        $('label[for="txtAnhCMNDMatTruoc"]').text("Thẻ CN (Mặt trước)");
        $('p[data-sample="txtAnhCMNDMatTruoc"]').attr('data-href', that.TuyChon.mauCnMatTruoc);
        $('p[data-sample="txtAnhCMNDMatSau"]').attr('data-href', that.TuyChon.mauCnMatSau);
    }
};

/**
 * Hàm ẩn các control của học sinh
 * @param {any} obj
 */
DangKyVe.prototype.ControlHocSinh = function (obj) {
    var that = this;
    if (!obj) {
        $('div[data-control="hocSinh"]').addClass('hidden');
        $('.mauDon').hide();
    }
    else {
        $('div[data-control="hocSinh"]').removeClass('hidden');
        $('label[for="txtAnhCMNDMatSau"]').text("Thẻ HSSV (Mặt sau)");
        $('label[for="txtAnhCMNDMatTruoc"]').text("Thẻ HSSV (Mặt trước)");
        $('p[data-sample="txtAnhCMNDMatTruoc"]').attr('data-href', that.TuyChon.mauHssvMatTruoc);
        $('p[data-sample="txtAnhCMNDMatSau"]').attr('data-href', that.TuyChon.mauHssvMatSau);
        $('.mauDon').show();
    }
};

/**
 * Hàm ẩn các control của một tuyến
 * @param {any} obj
 */
DangKyVe.prototype.ControlTuyen = function (obj) {
    if (!obj) {
        $('div[data-control="motTuyen"]').addClass('hidden');
    }
    else {
        $('div[data-control="motTuyen"]').removeClass('hidden');
    }
};

/**
 * Hàm ẩn các control của đăng ký tại nhà
 * @param {any} obj
 */
DangKyVe.prototype.ControlDangKyTaiNha = function (obj) {
    if (obj) {
        $('div[data-control="khongTaiNha"]').addClass('hidden');
        $('div[data-control="coTaiNha"]').removeClass('hidden');
    }
    else {
        $('div[data-control="coTaiNha"]').addClass('hidden');
        $('div[data-control="khongTaiNha"]').removeClass('hidden');
    }
};

/**
 * Hàm dùng để ẩn toàn bộ các control đưa form về trang thái load ban đầu
 */
DangKyVe.prototype.AnToanBo = function () {
    var that = this;
    that.ControlHocSinh(false);
    that.ControlCongNhan(false);
};

/**
 * Hàm ẩn hiện combobox loại đối tượng
 * @param {any} obj
 */
DangKyVe.prototype.AnHienComboLoaiDoiTuong = function (obj) {
    if (!obj) {
        $('#slLoaiDoiTuong').parentsUntil('div.row').parent().addClass('hidden');
        $('#tai_mau_don').addClass('hidden');
    }
    else {
        $('#slLoaiDoiTuong').parentsUntil('div.row').parent().removeClass('hidden');
        $('#tai_mau_don').removeClass('hidden');
    }
};
/**
 * Hàm xử lý Tab trên trang
 * @param {any} evt
 * @param {any} tabName
 */
DangKyVe.prototype.OpenTab = function (obj, tabId) {
    $('.tabcontent').each(function (i) {
        $(this).hide();
    });
    $('.tablinks').each(function (i) {
        $(this).removeClass('active');
    });
    tabId.show();
    obj.addClass('active');
};

/**
 * Hàm khởi tạo Dialog
 * @param {any} obj Obj vùng chứa dialog
 * @param {any} containerId id của vùng chứa khung hiển thị
 */
DangKyVe.prototype.InitDialog = function (obj, containerId) {
    obj.dialog(
        {
            autoOpen: false,
            width: 'auto',
            height: 'auto',
            position: { my: "left top", at: "left top", of: "#" + containerId }
        });
    this.Dialog = obj;
};
/**
 * Hàm mở cửa số
 * @param {any} url
 */
DangKyVe.prototype.OpenDialog = function (url) {
    var that = this;
    if (that.Dialog !== null) {
        that.Dialog.load(url);
        that.Dialog.dialog('open');
    }
};
/**
 * Hàm tính ngày nhận thẻ
 */
DangKyVe.prototype.TinhNgayNhanThe = function () {
    var that = this;
    var ngayHienTai = new Date();
    var ngayNhanTheSomNhat = new Date();
    var ngayNhanTheMuonNhat = new Date();
    var ngayKhongChe = {};

    ngayKhongChe.somNhat = (that.TuyChon && that.TuyChon.ngayNhanTheSomNhat) ? ngayNhanTheSomNhat.setDate(ngayHienTai.getDate() + eval(that.TuyChon.ngayNhanTheSomNhat)) : ngayNhanTheSomNhat.setDate(ngayHienTai.getDate() + 4);
    ngayKhongChe.muonNhat = (that.TuyChon && that.TuyChon.ngayNhanTheMuonNhat) ? ngayNhanTheMuonNhat.setDate(ngayHienTai.getDate() + eval(that.TuyChon.ngayNhanTheMuonNhat)) : ngayNhanTheMuonNhat.setDate(ngayHienTai.getDate() + 40);
    ngayKhongChe.ngayNghi = (that.TuyChon && that.TuyChon.ngayNghi) ? that.TuyChon.ngayNghi : ['0'];
    ngayKhongChe.ngayLe = (that.TuyChon && that.TuyChon.ngayLe) ? that.TuyChon.ngayLe : [ngayHienTai.getFullYear() + "-01-01"];

    return ngayKhongChe;
};
/**
 * Tính tháng mua tem theo ngày hiện tại
 * @param {any} ngayBatDauThang
 */
DangKyVe.prototype.TinhThangMuaTem = function (ngayBatDauThang) {
    var ngayHienTai = new Date().getDate();
    var thangHienTai = new Date().getMonth() + 1;
    if (ngayHienTai >= ngayBatDauThang) {
        //! Xử lý các trường hợp vượt qua tháng 12
        switch (thangHienTai) {
            case 10:
                return [thangHienTai + 1, thangHienTai + 2, 1];
            case 11:
                return [thangHienTai + 1, 1, 2];
            case 12:
                return [1,2,3];
            default:
                return [thangHienTai + 1, thangHienTai + 2, thangHienTai + 3];
        }
    }
    else {
        //! Xử lý các trường hợp vượt qua tháng 12
        switch (thangHienTai) {
            case 11:
                return [thangHienTai, thangHienTai + 1, 1];
            case 12:
                return [thangHienTai, 1, 2];
            default:
                return [thangHienTai, thangHienTai + 1, thangHienTai + 2];
        }
    }
};
/**
 * Hàm tính tuổi dựa trên chuỗi ngày sinh dạng ngay/thang/nam truyền vào
 * @param {any} chuoiNgaySinh đinh dạng theo ngay/thang/nam
 */
DangKyVe.prototype.TinhTuoiTuNgaySinh = function (chuoiNgaySinh) {
    var that = this;
    var homNay = new Date();
    var ngaySinh = that.ChuoiThanhNgay(chuoiNgaySinh);
    var tuoi = homNay.getFullYear() - ngaySinh.getFullYear();
    var m = homNay.getMonth() - ngaySinh.getMonth();
    if (m < 0 || (m === 0 && homNay.getDate() < ngaySinh.getDate())) {
        tuoi--;
    }
    return tuoi;
};
/**
 * Hàm kiểm tra tuổi theo tháng sinh
 * @param {any} chuoiNgaySinh
 */
DangKyVe.prototype.TinhTuoiTheoThang = function (chuoiNgaySinh) {
    var that = this;
    var homNay = new Date();
    var ngaySinh = that.ChuoiThanhNgay(chuoiNgaySinh);
    var thangTuoi = (homNay.getFullYear() - ngaySinh.getFullYear()) * 12;
    var m = homNay.getMonth() - ngaySinh.getMonth();
    return thangTuoi + m;
}
/*
 * Hàm chuyển chuỗi dạng ngay/thang/nam sang ngày
 */
DangKyVe.prototype.ChuoiThanhNgay = function (ngayThangNam) {
    var ngay = ngayThangNam.split("/");
    if (ngay.length < 3) {
        console.log('Lỗi ngày tháng');
    }
    return new Date(ngay[2], eval(ngay[1] - 1), ngay[0]);
};
/**
 * Hàm chuyển ngày thành chuỗi để hiện lên textbox
 * @param {any} ngayThangNam
 */
DangKyVe.prototype.NgayThanhChuoi = function (ngayThangNam) {
    var date = new Date(ngayThangNam);
    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
};
/**
 * Hàm tính phí tem
 */
DangKyVe.prototype.TinhPhiTem = function () {
    var that = this;
    var soThangMua = that.ThangDangKyMua.length;
    var phiTem = 0;
    var loaiDoiTuong = $('#rdDoiTuong_BinhThuong').is(":checked"); //! Kiểm tra nếu đối tượng bình thường được check
    //! Loại thẻ 1 tuyến
    if (that.LoaiThe === 1) {
        if (loaiDoiTuong) {
            phiTem = soThangMua * that.TuyChon.phiBinhThuongMotTuyen;
        } else {
            phiTem = soThangMua * that.TuyChon.phiUuTienMotTuyen;
        }
    }
    //! Loại thẻ liên tuyến
    else {
        if (loaiDoiTuong) {
            phiTem = soThangMua * that.TuyChon.phiBinhThuongLienTuyen;
        } else {
            phiTem = soThangMua * that.TuyChon.phiUuTienLienTuyen;
        }
    }
    return that.DinhDangTien(phiTem + "");
};
/**
 * Hàm định dạng dấu phảy cho tiền
 * @param {any} tien
 */
DangKyVe.prototype.DinhDangTien = function (tien) {
    return tien.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
};

/**
 * Thu nhỏ hình ảnh về 200px, 300px
 * @param {any} controlAnh Control chứa nội dung ảnh
 */
DangKyVe.prototype.DinhTyLeAnh = function (controlAnh, callback, type) {
    var context = document.getElementById('preview').getContext("2d");
    var img = new Image();
    img.onload = function () {
        var width = img.width;
        var height = img.height;
        var dtWidth = width;
        var dtHeight = height;
        if (type === 1) {
            dtHeight = 354;
            dtWidth = 236;
        }
        //else if (type === 2) {
        //    dtHeight = 300;
        //    dtWidth = 432;
        //}
        else if (type === 3) {
            dtHeight = 1200;
            dtWidth = 800;
        }

        //! Định kích thước Canvas
        context.canvas.width = dtWidth;
        context.canvas.height = dtHeight;
        //! Lưu ảnh vào canvas với tọa độ ảnh nguồn là 0,0, chiều cao, rộng là full. Ảnh đích là 0,0, chiều cao, rộng là 200/300
        context.drawImage(img, 0, 0, width, height, 0, 0, dtWidth, dtHeight);

        //! Lấy về data hình ảnh sau khi vẽ vào canvas để trả ra
        var hinhAnhSauThuNho = context.canvas.toDataURL();
        callback(controlAnh, 'data-content', hinhAnhSauThuNho);
    }
    img.src = controlAnh.attr('data-content');
};
/**
 * Hàm gán giá trị vào thuộc tính dùng trong callback
 * @param {any} control
 * @param {any} thuocTinh
 * @param {any} giaTri
 */
DangKyVe.prototype.CallBackGanGiaTri = function (control, thuocTinh, giaTri) {
    control.attr(thuocTinh, giaTri);
};
/**
 * Chỉ dùng để Test
 * @param {any} giaTri
 */
DangKyVe.prototype.InGiaTriTest = function (giaTri) {
    alert(giaTri);
};

/**
 * Hàm trả về data dạng base64 của hình ảnh. Hiện tại không dùng
 * @param {any} url
 * @param {any} callback
 */
DangKyVe.prototype.LayDuLieuAnh = function (url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        var reader = new FileReader();
        reader.onloadend = function () {
            callback(reader.result);
        }
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
};

DangKyVe.prototype.MakeFormData = function (form, action) {
    var that = this;
    try {
        var formData = new FormData();
        formData.append("hanhDong", action);
        formData.append("Captcha", form["txtCaptcha"].value);
        formData.append("FullName", that.HoTen);
        formData.append("BirthDay", that.NgaySinh);
        formData.append("PhoneNumber", that.DienThoai);
        formData.append("Address", that.DiaChi);
        formData.append("AddressName", that.TenDiaChi);
        formData.append("CardImg", that.AnhThe);
        formData.append("FrontImg", that.MatTruoc);
        formData.append("BackImg", that.MatSau);
        formData.append("AppImg", that.DonDangKy !== "" ? that.DonDangKy : that.DonXacNhan);
        formData.append("TicketType", that.DoiTuongUuTien);
        formData.append("FleetType", that.LoaiThe);
        formData.append("RegisterFleet", that.Tuyen);
        formData.append("SchoolName", that.TenTruongHoc);
        formData.append("SchoolYear", that.NienKhoa);
        formData.append("SchoolId", that.TruongHoc);
        formData.append("IndustryName", that.TenKhuCongNghiep);
        formData.append("IndustryId", that.KhuCongNghiep);
        formData.append("ReceiverDistrictId", that.Quan);
        formData.append("ReceiverAddress", that.DiaChiNhanThe);
        formData.append("ReceiverTicketPlace", that.NoiNhanThe);
        formData.append("ReceiverDate", that.NgayNhanThe);
        formData.append("RegistryStampMonths", that.ThangDangKyMua);
        formData.append("IsShipHome", that.NhanTaiNha);
        formData.append("ClientID", that.ClientID);
        formData.append("DataAnhLamThe", that.DuLieuAnhThe);
        formData.append("DataMatTruoc", that.DuLieuMatTruoc);
        formData.append("DataMatSau", that.DuLieuMatSau);
        formData.append("DataDangKy", that.DuLieuDonDangKy);
        formData.append("DataXacNhan", that.DuLieuDonXacNhan);
        formData.append("MaTicket", that.MaTicket);
        formData.append("TrangThaiCu", that.TrangThaiCu);
        return formData;
    } catch (ex) {
        return null;
    }
}
DangKyVe.prototype.GetRandomText = function (length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 1; i <= length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
};
