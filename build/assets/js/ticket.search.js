/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2021. MIT licensed.
 *//**
 * Đối tượng đăng ký vé tháng
 * Tác giả: SonKT
 * Ngày tạo: 227/03/2018
 * Update lần cuối 03/04/2018 By SonKT
 * Mô tả: Đối tượng này dùng để quản lý việc đăng ký vé tháng
 */
"use strict";

function TraCuu() {
  this.AnhThe = null; //! Lưu thông tin ảnh thẻ từ Filemanager trả về

  this.MatTruoc = null; //! Lưu thông tin ảnh chứng minh nhân dân hoặc ảnh thẻ công nhân hoặc thẻ sinh viên mặt trước từ filemanager trả về

  this.MatSau = null; //! Lưu thông tin ảnh chứng minh nhân dân hoặc ảnh thẻ công nhân hoặc thẻ sinh viên mặt sau từ filemanager trả về

  this.DonDangKy = ""; //! Lưu link file đăng ký của học sinh/sinh viên

  this.DonXacNhan = ""; //! Lưu link file đăng ký của công nhân

  this.HoTen = ""; //! Họ tên người đăng ký

  this.NgaySinh = new Date(); //! Ngày sinh người đăng ký

  this.DienThoai = ""; //! Điện thoại người đăng ký

  this.TruongHoc = ""; //! ID Trường học của sinh viên hoặc học sinh

  this.TenTruongHoc = ""; //! Tên trường học

  this.NienKhoa = ""; //! Niên khóa của sinh viên hoặc học sinh

  this.KhuCongNghiep = ""; //! ID khu công nghiệp

  this.TenKhuCongNghiep = ""; //! Tên khu công nghiệp

  this.DiaChi = ""; //! Địa chỉ của người đăng ký

  this.DoiTuongUuTien = 0; //! Loại đối tượng ưu tiên: 0 - bình thường, 1 - Học Sinh, 2 - Công nhân, 3 - Người cao tuổi

  this.LoaiThe = 1; //! Loại thẻ 1 - Một tuyến, 2 - Liên tuyến

  this.Tuyen = ""; //! Mã tuyến

  this.NoiNhanThe = ""; //! Mã nơi nhận thẻ

  this.NgayNhanThe = new Date(); //! Ngày nhận thẻ

  this.ThangDangKyMua = []; //! Các tháng đăng ký mua

  this.Quan = ""; //! Mã quận khi đăng ký liên tuyến

  this.NhanTaiNha = false; //! Đăng ký nhận tại nhà True - Nhận tại nhà, False - Không nhận tại nhà

  this.DiaChiNhanThe = ""; //! Địa chỉ nhận thẻ

  this.ClientID = ""; //! ID của thiết bị nếu là App mobile

  this.Dialog = null; //! Dialog của cửa sổ chỉnh sửa hình ảnh

  this.TrangThaiVe = []; //! Trạng thái vé

  this.TuyChon = null; //! Tùy chọn được gán giá trị khi khởi tạo đối tượng
}
/**
 * Hàm khởi tạo tra cứu
 * @param {any} tuyChon
 */


TraCuu.prototype.khoiTao = function (tuyChon) {
  var that = this;

  if (tuyChon) {
    that.TuyChon = tuyChon;
  }

  var length = tuyChon.danhSachTrangThaiVe.length;
  that.TrangThaiVe.push(' ');

  if (tuyChon.danhSachTrangThaiVe) {
    for (var j = 0; j < length; j++) {
      that.TrangThaiVe.push(tuyChon.danhSachTrangThaiVe[j]);
    }
  } //! Khởi tạo control ngày sinh


  $('#dtpNgaySinh').datetimepicker({
    locale: 'vi',
    format: 'DD/MM/YYYY',
    useCurrent: false,
    maxDate: new Date()
  });
  $("#frmTimKiem").validate({
    rules: {
      txtHoTen: {
        required: {
          depends: function () {
            return $.trim($(this).val()) === "";
          }
        },
        maxlength: 150,
        minlength: 6
      },
      txtNgaySinh: {
        required: {
          depends: function () {
            return $.trim($(this).val()) === "";
          }
        }
      },
      txtDienThoai: {
        required: {
          depends: function () {
            return $.trim($(this).val()) === "";
          }
        },
        maxlength: 12,
        number: true
      }
    },
    messages: {
      txtHoTen: {
        required: "Bạn phải nhập đầy đủ họ tên",
        maxlength: "Tối đa 150 ký tự",
        minlength: "Tối thiểu 6 ký tự"
      },
      txtNgaySinh: {
        required: "Ngày sinh không để trống"
      },
      txtDienThoai: {
        required: "Điện thoại không để trống",
        maxlength: "Điện thoại không quá 12 ký tự",
        number: "Điện thoại chỉ chứa ký tự số"
      }
    },
    errorElement: "em",
    highlight: function (element, errorClass, validClass) {
      $(element).parents(".form-group").addClass("has-error").removeClass("has-success");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).parents(".form-group").addClass("has-success").removeClass("has-error");
    },
    errorPlacement: function (error, element) {
      error.addClass("help-block col-sm-9 col-xs-8 col-md-offset-3 col-xs-offset-4");
      error.insertAfter(element.parents('.col-sm-9'));
    },
    success: function (label, element) {
      label.parent().removeClass('error');
      label.remove();
    },
    submitHandler: function (form) {
      //! Gọi hàm lấy giá trị từ form vào đối tượng
      that.LayGiaTriForm(); //! Tạo formData để đẩy lên server

      var formData = new FormData();
      formData.append("hanhDong", "timHoSo");
      formData.append("Captcha", form["txtCaptcha"].value);
      formData.append("FullName", that.HoTen);
      formData.append("BirthDay", that.NgaySinh);
      formData.append("PhoneNumber", that.DienThoai);
      $('.ket-qua-tim-kiem').hide();
      $.ajax({
        url: "ActionHandler.ashx",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (result) {
          var ketQua = $.parseJSON(result); //grecaptcha.reset();

          $('#imgCatchaValidate').attr('src', '/TicketRegister/captchaImg.aspx?ver=' + that.GetRandomText(5));
          $("#txtCaptcha").val("");
          $('.ket-qua-tim-kiem').has('hr').show();
          $('.noi-dung-can-bo-sung').hide();

          switch (ketQua.st) {
            case 1:
              //! Tìm thấy
              //! Giá trị trạng thái
              $('.ket-qua-tim-kiem').has('#lblTrangThai').show();
              $("#lblTrangThai").text(that.TrangThaiVe[ketQua.ticket.State]);
              $('.ket-qua-tim-kiem').has('#lblNgayNhanThe').show();
              $('#lblNgayNhanThe').text(moment(ketQua.ticket.ReceiverDate).format('DD/MM/YYYY'));
              $('.ket-qua-tim-kiem').has('#lblDiemNhanThe').show();
              $('#lblDiemNhanThe').text(ketQua.ticket.ReceiverAddress); //! Các trường hợp đặc biệt cần hiển thị thông báo

              switch (ketQua.ticket.State) {
                case 1:
                  $('.ket-qua-tim-kiem').has('#lblNoiDungThongBao').show();

                  if (that.TuyChon.isM == true) {
                    $('#lblNoiDungThongBao').html('Để thay đổi thông tin vé tháng trong hồ sơ đăng ký hoặc hủy hồ sơ đã đăng ký, quý khách vui lòng truy cập vào mục Đăng Ký và nhập 3 thông tin: Họ tên, điện thoại, ngày sinh.');
                  } else {
                    $('#lblNoiDungThongBao').html('Để thay đổi thông tin vé tháng trong hồ sơ đăng ký hoặc hủy hồ sơ đã đăng ký, xin quý khách hàng vui lòng nhấp chuột vào <a href="/Ticket/Default.aspx?hoTen=' + that.HoTen + '&ngaySinh=' + that.NgaySinh + '&dienThoai=' + that.DienThoai + '">ĐĂNG KÝ VÉ THÁNG</a>');
                  }

                  break;

                case 2:
                  $('.noi-dung-can-bo-sung').show();
                  $('.ket-qua-tim-kiem').has('#lblNoiDungThongBao').show();
                  $('#lblNoiDungBoSung').text(ketQua.ticket.NoteForCustomer);

                  if (that.TuyChon.isM == true) {
                    $('#lblNoiDungThongBao').html('Để thay đổi thông tin vé tháng trong hồ sơ đăng ký hoặc hủy hồ sơ đã đăng ký, quý khách vui lòng truy cập vào mục Đăng Ký và nhập 3 thông tin: Họ tên, điện thoại, ngày sinh.');
                  } else {
                    $('#lblNoiDungThongBao').html('Để thay đổi thông tin vé tháng trong hồ sơ đăng ký hoặc hủy hồ sơ đã đăng ký, xin quý khách hàng vui lòng nhấp chuột vào <a href="/Ticket/Default.aspx?hoTen=' + that.HoTen + '&ngaySinh=' + that.NgaySinh + '&dienThoai=' + that.DienThoai + '">ĐĂNG KÝ VÉ THÁNG</a>');
                  }

                  break;

                case 12:
                case 13:
                case 15:
                case 16:
                  if (ketQua.ticket.ReasonCancelForCustomer) {
                    $('.ket-qua-tim-kiem').has('#lblNoiDungThongBao').show();
                    $('#lblNoiDungThongBao').text("Lý do hủy: " + ketQua.ticket.ReasonCancelForCustomer);
                  }

                  break;

                case 5:
                  $('.ket-qua-tim-kiem').has('#lblNoiDungThongBao').show();
                  $('#lblNoiDungThongBao').text("Xin quý khách vui lòng đến điểm bán vé đã đăng ký để nhận thẻ");
                  break;

                case 6:
                case 9:
                case 10:
                case 11:
                  //! Các trạng thái này không Show thông báo.
                  break;
                //case 15:
                //    $('.ket-qua-tim-kiem').has('#lblNoiDungThongBao').show();
                //    $('#lblNoiDungThongBao').text("Hệ thống tự động hủy đơn quá 35 ngày");
                //    break;

                case 14:
                  if (ketQua.ticket.ReasonCancelCustomer) {
                    $('.ket-qua-tim-kiem').has('#lblNoiDungThongBao').show();
                    $('#lblNoiDungThongBao').text("Lý do hủy: " + ketQua.ticket.ReasonCancelCustomer);
                  }

                  break;

                default:
                  if (ketQua.ticket.IsShipHome) {
                    $('.ket-qua-tim-kiem').has('#lblNoiDungThongBao').show();
                    $('#lblNoiDungThongBao').text("Thẻ sẽ được giao đến địa chỉ khách hàng đã đăng ký");
                  }

                  break;
              }

              break;

            case 0:
              //! Mã captcha không đúng hoặc hết hiệu lực
              $('.ket-qua-tim-kiem').has('#lblNoiDungThongBao').show();
              $('#lblNoiDungThongBao').text('Mã xác thực không đúng hoặc hết hiệu lực');
              break;

            case -1:
              //! Không tìm thấy dữ liệu
              $('.ket-qua-tim-kiem').has('#lblNoiDungThongBao').show();
              $('#lblNoiDungThongBao').text('Không tìm thấy thông tin đăng ký phù hợp với nội dung tra cứu này');
              break;

            case -2:
              //! Có lỗi khi thực hiện
              $('.ket-qua-tim-kiem').has('#lblNoiDungThongBao').show();
              $('#lblNoiDungThongBao').text('Có lỗi khi thực hiện tìm kiếm. Vui lòng thử lại sau');
              break;
          }
        },
        error: function (xhr, status) {
          console.log("Có lỗi xảy ra: " + status);
        }
      });
    }
  });
};
/**
 * Hàm này dùng để lấy giá trị trên form vào thuộc tính của đối tượng
 */


TraCuu.prototype.LayGiaTriForm = function () {
  var that = this;
  that.HoTen = $("#txtHoTen").val();
  that.NgaySinh = $("#txtNgaySinh").val();
  that.DienThoai = $("#txtDienThoai").val();
};

TraCuu.prototype.GetRandomText = function (length) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 1; i <= length; i++) text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
};